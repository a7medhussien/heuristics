//
//  MessageBodyNotification.swift
//  Heuristics
//
//  Created by Magdy Zamel on 4/16/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import Foundation

public class MessageBodyNotification {


    public var appId :String!
    public var  contentMessage: String
    public var  senderName: String!
    public var value : String!



    public init(userId:String,contentMessage:String,senderName:String){
        appId  = "c6c7ba83-6739-4713-9a56-e0bb2a414f06"
        self.contentMessage = contentMessage
        value = userId
        self.senderName = senderName
    }
    func toDic() -> [String: Any]  {
        let filters:[[String: Any]]  = [
            ["field": "tag", "key": "userId", "relation": "=", "value": "\(value!)"]  ]
                let dic:[String : Any] = [
                    "app_id": "\(Singleton.oneSignalAppId)",
                     "filters": filters,
                     "data": ["foo": "bar"],
                     "contents": ["en": "\(contentMessage)"],
                     "content_available":true,
                     "headings":["en":"\(senderName!)"]
            ]
        return dic
    }
}
