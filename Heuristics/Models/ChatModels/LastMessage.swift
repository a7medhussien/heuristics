//
//  LastMessageModel.swift
//  Nauta
//
//  Created by Magdy Zamel on 4/16/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import Foundation
import ObjectMapper

public class LastMessage: Mappable {

    public var ownerUserID : String!
    public var msg : String!
    public var receiverId : String!
    public var senderId : String!
    public var senderName : String!
    public var receiverName : String!
    public var timeStamp : String!



    //MARK: Decodable


    required public  init?(map: Map) {}

    public func mapping(map: Map) {
        ownerUserID <- map["ownerUserID"]
        msg <- map["msg"]
        receiverId <- map["receiverId"]
        senderId <- map["senderId"]
        senderName <- map["senderName"]
        receiverName <- map["receiverName"]

        var timeResult:Double!
        timeResult <- map["timeStamp"]
        let date = NSDate(timeIntervalSince1970:TimeInterval(timeResult/1000))
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "dd MMM yyyy"
        timeStamp = dateFormatter.string(from: date as Date)
    }




}
