
/**
 * Copyright (c) 2016 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import MapKit
import CoreLocation
import ObjectMapper

struct UserKey {
    static let latitude = "latitude"
    static let longitude = "longitude"
    static let name = "name"
    static let email = "date"
    static let userID = "userID"
    static let userName = "userName"
    static let phoneNumber = "phoneNumber"
    static let gender = "gender"
}

class User: Mappable {
    
    var lat: Double?
    var long: Double?
    var name: String?
    var email: String?
    var userID: String?
    var userName: String?
    var imageUrl: String?
    var activeTime: String?
    var phoneNumber: String?
    var gender: Int?
    var followers = [FollowModel?]()
    var following = [FollowModel?]()
    
    //presentation data
    var distanceFromCurrentUser: Double?
    var img: UIImage?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        userID <- map["userId"]
        userName <- map["username"]
        name <- map["Name"]
        email <- map["Email"]
        imageUrl <- map["imageUrl"]
        activeTime <- map["ActiveTime"]
        followers <- map["followers"]
        following <- map["following"]
        lat <- map["Lat"]
        long <- map["Long"]
        phoneNumber <- map["phoneNumber"]
        gender <- map["gender"]
    }
    
    init() {
        self.userID = ""
        self.name = ""
        self.email = ""
        self.userID = ""
        self.userName = ""
        self.phoneNumber = ""
        self.lat = 0
        self.long = 0
        self.gender = 1
    }
    
    init( name: String ,email:String, userID: String, userName: String, lat: Double, long: Double) {
        self.name = name
        self.email = email
        self.userID = userID
        self.userName = userName
        self.lat = lat
        self.long = long
    }

    func getDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: activeTime ?? "") ?? Date()
    }
}
