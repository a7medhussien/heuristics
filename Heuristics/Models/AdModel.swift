//
//	ImageData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import UIKit

struct AdModel : Codable {
     public var type : Int?
     public var category : String?
     public var title : String?
     public var details : String?
     public var brandName : String?

     public var locationDescription : String?
     public var price : String?
     public var websiteURl : String?
     public var threeSixtyURLs : String?
     public var lat : Double?
     public var lng : Double?
     public var imagesUrls : [String]?



     enum CodingKeys: String, CodingKey {
          case type = "type"
          case category = "category"
          case title = "title"
          case details = "details"
          case brandName = "brandName"
          case locationDescription = "locationDescription"
          case price = "price"
          case websiteURl = "websiteURl"
          case lat = "lat"
          case lng = "lng"
          case imagesUrls = "imagesUrls"
          case threeSixtyURLs = "threeSixtyURLs"


     }
     init(from decoder: Decoder) throws {
          let values = try decoder.container(keyedBy: CodingKeys.self)

          type = try values.decodeIfPresent(Int.self, forKey: .type)

          brandName = try values.decodeIfPresent(String.self, forKey: .brandName)
          category = try values.decodeIfPresent(String.self, forKey: .category)
          details = try values.decodeIfPresent(String.self, forKey: .details)
          title = try values.decodeIfPresent(String.self, forKey: .title)
          locationDescription = try values.decodeIfPresent(String.self, forKey: .locationDescription)
          websiteURl = try values.decodeIfPresent(String.self, forKey: .websiteURl)
          price = try values.decodeIfPresent(String.self, forKey: .price)
          threeSixtyURLs = try values.decodeIfPresent(String.self, forKey: .threeSixtyURLs)


          lat = try values.decodeIfPresent(Double.self, forKey: .lat)
          lng = try values.decodeIfPresent(Double.self, forKey: .lng)
          imagesUrls = try values.decodeIfPresent([String].self, forKey: .imagesUrls)
     }
     func encode(to encoder: Encoder) throws {
          var container = encoder.container(keyedBy: CodingKeys.self)
          try container.encode(type, forKey: .type)
          try container.encode(category, forKey: .category)
          try container.encode(brandName, forKey: .brandName)

          try container.encode(title, forKey: .title)
          try container.encode(details, forKey: .details)
          try container.encode(locationDescription, forKey: .locationDescription)
          try container.encode(price, forKey: .price)
          try container.encode(websiteURl, forKey: .websiteURl)
          try container.encode(threeSixtyURLs, forKey: .threeSixtyURLs)

          try container.encode(lat, forKey: .lat)
          try container.encode(lng, forKey: .lng)
          try container.encode(imagesUrls, forKey: .imagesUrls)
     }

     init(){}

}
struct AdsModel : Codable {

     var ads = [AdModel]()
     var timeStamp : Double?
     var uuid: String?
     var stop: Bool?


     enum CodingKeys: String, CodingKey {
          case uuid = "uuid"
          case timeStamp = "timeStamp"
          case ads = "ads"
          case stop = "stop"
     }
     init(from decoder: Decoder) throws {
          let values = try decoder.container(keyedBy: CodingKeys.self)
          ads = (try values.decodeIfPresent([AdModel].self, forKey: .ads))!
          timeStamp = try values.decodeIfPresent(Double.self, forKey: .timeStamp)
          uuid = try values.decodeIfPresent(String.self, forKey: .uuid)
          stop = try values.decodeIfPresent(Bool.self, forKey: .stop)

     }

     init(){}

}
