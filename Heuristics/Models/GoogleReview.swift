//
//  GoogleReview.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/12/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import SwiftyJSON

class GoogleReview {
    
    let authorName: String
    let profilePhotoURL: String
    var reviewContent: String
    var rating: Double
    var time: String
    
    init(dictionary: [String: Any])
    {
        
        let json = JSON(dictionary)
        authorName = json["author_name"].stringValue
        profilePhotoURL = json["profile_photo_url"].stringValue
        if json["text"].stringValue.isEmpty {
            reviewContent = "No review"
        }
        reviewContent = json["text"].stringValue
        rating = json["rating"].doubleValue
        time = json["relative_time_description"].stringValue
      
    }
}
