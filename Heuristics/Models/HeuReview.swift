//
//  HeuReview.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/20/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import SwiftyJSON

class HeuReview {
    
    var placeID: String
    var authorName: String
    var reviewContent: String
    var rating: Double
    var time: String
    
    init(dictionary: [String: Any])
    {
        
        let json = JSON(dictionary)
        placeID = json["placeID"].stringValue
        authorName = json["authorName"].stringValue
        reviewContent = json["review"].stringValue
        rating = json["rating"].doubleValue
        time = json["time"].stringValue
        
    }
}
