//
//  GooglePlaceDetails.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/12/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import SwiftyJSON

class GooglePlaceDetails {
    let name: String
    let address: String
    let placeID: String
    let rating: Double
    let mapURL: String
    let website: String
    let phoneNumber: String
    let coordinate: CLLocationCoordinate2D
    var photoReference: String?
    var photo: UIImage?
    var reviews: [GoogleReview]
    var photosURLs: [String]!
    
    init(dictionary: [String: Any])
    {
        let json = JSON(dictionary)
        
        name = json["name"].stringValue
        address = json["vicinity"].stringValue
        placeID = json["place_id"].stringValue
        rating = json["rating"].doubleValue
        mapURL = json["url"].stringValue
        phoneNumber = json["formatted_phone_number"].stringValue
        website = json["website"].stringValue
        
        let lat = json["geometry"]["location"]["lat"].doubleValue as CLLocationDegrees
        let lng = json["geometry"]["location"]["lng"].doubleValue as CLLocationDegrees 
        coordinate = CLLocationCoordinate2DMake(lat, lng)
        
        photoReference = json["reference"].stringValue
        
        var reviews = [GoogleReview]()
        if let placeReviews = json["reviews"].arrayObject {
            placeReviews.forEach {
                let review = GoogleReview(dictionary: $0 as! [String : Any])
                reviews.append(review)
            }
        }
        self.reviews = reviews
        
        var photosURLs = [String]()
        if let placePhotos = json["photos"].arrayObject {
           placePhotos.forEach {
                let photoDic = $0 as! [String : Any]
                let photoJSON = JSON(photoDic)
                let photoRef = photoJSON["photo_reference"].stringValue
            
                photosURLs.append(photoRef)
            }
        }
        self.photosURLs = photosURLs
        
    }
}

