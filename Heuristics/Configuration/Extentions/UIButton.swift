//
//  UIButton.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/21/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

extension UIButton {
    
    func follow(){
        self.isSelected = true
        self.setTitle("Following", for: .selected)
        self.setBackgroundColor(UIColor.darkGreen, forState: .normal)
    }
    
    func unfollow(){
        self.isSelected = true
        self.setTitle("Follow", for: .selected)
        self.setBackgroundColor(UIColor.watermelon2, forState: .normal)
    }
    
}
