//
//  UITableViewCell.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/19/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    @IBInspectable var normalBackgroundColor: UIColor? {
        get {
            return backgroundView?.backgroundColor
        }
        set(color) {
            let background = UIView()
            background.backgroundColor = color
            backgroundView = background
        }
    }
    
    @IBInspectable var selectedBackgroundColor: UIColor? {
        get {
            return selectedBackgroundView?.backgroundColor
        }
        set(color) {
            let background = UIView()
            background.backgroundColor = color
            selectedBackgroundView = background
        }
    }
}
