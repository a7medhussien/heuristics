//
//  UITextField.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/19/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

extension UITextField {
    var validator: Validator {
        get { return Validator(textField: self) }
    }
}

struct Validator {
    
    var textField: UITextField?
    
    init(textField: UITextField) {
        self.textField = textField
    }
    
    func isValidEmail() -> String? {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let validMessage = "Please enter a valid email address to complete your registeration request"
        return emailTest.evaluate(with: textField?.text ?? "") ? nil : validMessage
    }
    
    func isValidePassword() -> String? {
        let passwordRegEx = "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        let validMessage = "Password must be at least 8 characters long, have at least one uppercase letter and contains alphanumeric"
        return passwordTest.evaluate(with: textField?.text ?? "") ? nil : validMessage
    }
    
    func isNonEmpty() -> String? {
        let validMessage = "Please complete all empty fields to complete your registeration request"
        let text = textField?.text ?? ""
        let trimmedString = text.trimmingCharacters(in: .whitespaces)
        return trimmedString.isEmpty ? validMessage : nil
    }
    
    func verifyPassword(With textField: UITextField) -> String? {
        let validMessage = "Please make sure that the password field match verify password"
        return (self.textField?.text != textField.text) ? validMessage : nil
    }
}
