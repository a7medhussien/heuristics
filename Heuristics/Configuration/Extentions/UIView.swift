//
//  UIView.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

extension UIView {
    
    func startOpequeLoadingView(){
        
        self.endEditing(true)
        self.layoutIfNeeded()
        
        for view in self.subviews {
            if view.tag == 1024 + 8 {
                UIView.animate(withDuration: 0.3) {
                    view.isHidden = false
                    view.alpha = 1
                }
                return
            }
        }
        
        let loadingView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        loadingView.isUserInteractionEnabled = false
        loadingView.isHidden = true
        loadingView.alpha = 0
        loadingView.tag = 1024 + 8
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        indicator.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        indicator.startAnimating()
        loadingView.addSubview(indicator)
        
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Please wait"
        label.textColor = UIColor.white
        label.sizeToFit()
        label.center = CGPoint(x: self.frame.width/2, y: (self.frame.height/2) + 35)
        loadingView.addSubview(label)
        
        self.addSubview(loadingView)
        UIView.animate(withDuration: 0.3) {
            loadingView.isHidden = false
            loadingView.alpha = 1
        }
        
    }
    
    func stopOpequeLoadingView(){
        for view in self.subviews {
            if view.tag == 1024 + 8 {
                UIView.animate(withDuration: 0.3) {
                    view.isHidden = true
                    view.alpha = 0
                }
                return
            }
        }
    }
    
}
