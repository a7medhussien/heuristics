//
//  UIColor.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/7/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var mainAppColor: UIColor {
        return UIColor(red: 241/255.0, green: 241/255.0, blue: 241/255.0, alpha: 1.0)
    }
    
    static var fedora: UIColor {
        return UIColor(red: 130/255.0, green: 115/255.0, blue: 130/255.0, alpha: 1.0)
    }
    
    static var watermelon: UIColor {
        return UIColor(red: 44/255.0, green: 62/255.0, blue: 80/255.0, alpha: 1.0)
    }
    
    static var watermelon2: UIColor {
        return UIColor(red: 246.0/255.0, green: 21.0/255.0, blue: 77.0/255.0, alpha: 1.0)
    }
    
    static var darkGreen: UIColor {
        return UIColor(red: 0/255.0, green: 100/255.0, blue: 0/255.0, alpha: 1.0)
    }
}
