//
//  String Extension.swift
//  Heuristics
//
//  Created by Magdy Zamel on 6/25/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import Foundation

extension StringProtocol {
    var ascii: [UInt32] {
        return unicodeScalars.filter{$0.isASCII}.map{$0.value}
    }
}
extension Character {
    var ascii: UInt32? {
        return String(self).unicodeScalars.filter{$0.isASCII}.first?.value
    }
}
