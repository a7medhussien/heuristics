//
//  Utilities.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/7/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import Foundation
import UIKit
import MapKit

let URL_IMAGE_PREFIX = "https://firebasestorage.googleapis.com/v0/b/heuristics-194822.appspot.com/o/images%2F"
var CURRENT_USER:User? {
    didSet{
        debugPrint("Set")
    }
}
enum followViewType {
    case followers
    case following
}

func checkFollowState(withUid uid: String?) -> Bool {
    for user in CURRENT_USER?.following ?? [] {
        if let user = user {
            if user.userID == uid {
                return true
            }
        }
    }
    return false
}

extension UIView {
    
    func takeScreenShot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func getMainScreenWidth() -> CGFloat {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width

        return screenWidth
    }
    
    func getMainScreenHeight() -> CGFloat {
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        
        return screenHeight
    }
}

let imageCache = NSCache<AnyObject, AnyObject>()
let appColor = UIColor(red: 238/255, green: 8/255, blue: 78/255, alpha: 1.0)
let appGray = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
let myMsgColor = UIColor(red: 130/255, green: 115/255, blue: 130/255, alpha: 1.0)

extension UIImageView {
    func loadImageFrom(url:URL,placeholder:UIImage) {
        self.image = placeholder
        if let cachedImage = imageCache.object(forKey: url as AnyObject) as? UIImage {
            self.image = cachedImage
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let res:HTTPURLResponse = response as? HTTPURLResponse {
                if (error != nil || res.statusCode != 200) {
                    return
                }
                
                DispatchQueue.main.async {
                    if let downloadedImage = UIImage(data: data!) {
                        imageCache.setObject(downloadedImage, forKey: url as AnyObject)
                        self.image = UIImage(data: data!)
                        
                    }
                }
            }
            }.resume()
    }
}

enum VendingMachineError:Error {
    case valueNotFounds
}
extension MKMapView {
    func zoomToUserLocation() {
        
        guard let coordinate = userLocation.location?.coordinate else { return }
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 10000, 10000)
        setRegion(region, animated: true)
    }
}

extension Date{
    func getStringFromDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        return formatter.string(from: self as Date)
    }
    
    func getElapsedInterval() -> String {
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: self, to: Date())
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year" :
                "\(year)" + " " + "years"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month" :
                "\(month)" + " " + "months"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day" :
                "\(day)" + " " + "days"
        } else if let hour = interval.hour, hour > 0{
            return hour == 1 ? "\(hour)" + " " + "hour" :
                "\(hour)" + " " + "hours"
        } else if let minute = interval.minute, minute > 0{
            return minute == 1 ? "\(minute)" + " " + "minute" :
                "\(minute)" + " " + "minutes"
        }else{
            return "a moment ago"
        }
    }
}
extension NSDictionary {
    func getValueForKey<T>(key:String,callback:T)  -> T{
        guard let value  = self[key] as? T else{
            return callback}
        return value
    }
    func getValueForKey<T>(key:String) throws -> T{
        guard let value  = self[key] as? T else{throw VendingMachineError.valueNotFounds}
        return value
    }
}

extension UIViewController{
    func showAlertWithTitle(title:String?,message:String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showFailedAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Failed", message: "Couldn't Get Your Data", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: .default, handler: { (action) in
                self.viewDidLoad()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                self.view.isUserInteractionEnabled = false
                self.backTapped(nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showNoData(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Sorry", message: "There's no data", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
