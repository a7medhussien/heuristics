//
//  AlertsVC.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 4/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase

class AlertsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var reminders: [Geotification] = []
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupData()
    }

    
    // MARK: Private Helpers
    fileprivate func setupView() {
        self.title = "Reminders"
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        let addGeoAlert = UIBarButtonItem(barButtonSystemItem: .add,
                                             target: self,
                                             action: #selector(addAlertPressed(_:)))
        self.navigationItem.rightBarButtonItem = addGeoAlert
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
        let alertRightCell = UINib(nibName: "AlertRightCell", bundle: nil)
        tableView.register(alertRightCell, forCellReuseIdentifier: "AlertRightCell")
        
        let alertLeftCell = UINib(nibName: "AlertLeftCell", bundle: nil)
        tableView.register(alertLeftCell, forCellReuseIdentifier: "AlertLeftCell")
    }
    
    func addAlertPressed(_ sender: UIButton) {
     
        let vc = AlertsStoryboard.initialViewController()
        let addAlertVC = vc.viewControllers[0] as! AddAlertVC
        addAlertVC.delegate = self
        self.present(vc, animated: true, completion: nil)
    }

   
   
    
    // MARK: Functions that update the model/associated views with geotification changes

    
    
    func remove(geotification: Geotification) {
        if let indexInArray = reminders.index(of: geotification) {
            reminders.remove(at: indexInArray)
            
        }
    }
    
    
    func region(withGeotification geotification: Geotification) -> CLCircularRegion {
        // 1
        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
        // 2
        region.notifyOnEntry = (geotification.eventType == .onEntry)
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }
    
    func startMonitoring(geotification: Geotification) {
        // 1
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            self.showAlertWithTitle(title: "Error", message: "Geofencing is not supported on this device!")
            return
        }
        // 2
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            self.showAlertWithTitle(title: "Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
        }
        // 3
        let region = self.region(withGeotification: geotification)
        // 4
        locationManager.startMonitoring(for: region)
    }
    
    func stopMonitoring(geotification: Geotification) {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geotification.identifier else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
    }
    
    fileprivate func setupData() {
        let userId = Auth.auth().currentUser?.uid ?? ""
        APIManager.defaultManager.getUserLocationReminders(withUserId: userId) { (reminders, status, msg) in
            if status {
                if let reminders = reminders {
                    self.reminders = reminders
                    self.tableView.reloadData()
                    
                    for message in self.reminders {
                        self.startMonitoring(geotification: message)
                    }
                    
                }
            }
        }
    }

}
extension AlertsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let offerDetailViewController = OfferDetailViewController()
//        offerDetailViewController.modalPresentationStyle = .overFullScreen
//        self.present(offerDetailViewController, animated: false, completion: nil)
    }
    
   func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let removeGeo = self.reminders[indexPath.row]
            if let indexInArray = reminders.index(of: removeGeo) {
                reminders.remove(at: indexInArray)
                APIManager.defaultManager.removeLocationReminder(withUserId: CURRENT_USER?.userID ?? "", reminder:  removeGeo, handler: {
                    (msg, status) in
                    if !status {
                        self.showAlertWithTitle(title: "Failue", message: msg ?? "")
                        return
                    }
                })
            }
        }
    }
}

extension AlertsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reminders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentGeo = self.reminders[indexPath.row]
        let titleAndNotes = currentGeo.note.split("[-]|[-]")
        let title = titleAndNotes[0]
        let notes = titleAndNotes[1]
        
        if indexPath.row % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlertLeftCell",
                                                     for: indexPath) as! AlertLeftCell
            cell.title.text = title
            cell.note.text = notes
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlertRightCell",
                                                     for: indexPath) as! AlertRightCell
            cell.title.text = title
            cell.note.text = notes
            return cell
        }
    }
}

// MARK: AddGeotificationViewControllerDelegate
extension AlertsVC: AddAlertVCDelegate {
   

    func addAlertVC(controller: AddAlertVC, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: EventType, senderName: String) {
    
        controller.dismiss(animated: true, completion: nil)
        
        let clampedRadius = min(radius, locationManager.maximumRegionMonitoringDistance)
        let geotification = Geotification(coordinate: coordinate, radius: clampedRadius, identifier: identifier, note: note, eventType: eventType, senderName:senderName, senderId: CURRENT_USER?.userID ?? "")
        
        startMonitoring(geotification: geotification)
    }
    
    
    
}

// MARK: - Location Manager Delegate
extension AlertsVC: CLLocationManagerDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
    
}
