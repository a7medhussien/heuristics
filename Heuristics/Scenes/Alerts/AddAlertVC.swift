//
//  AddAlertVC.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 4/26/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import MapKit
import Firebase

protocol AddAlertVCDelegate {
    func addAlertVC(controller: AddAlertVC, didAddCoordinate coordinate: CLLocationCoordinate2D,
                                        radius: Double, identifier: String, note: String, eventType: EventType,senderName:String)
}

class AddAlertVC: UITableViewController {
    
    @IBOutlet var addButton: UIBarButtonItem!
    @IBOutlet var zoomButton: UIBarButtonItem!
    @IBOutlet weak var eventTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var radiusTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var noteTextField: UITextField!
    @IBOutlet weak var mapView: MKMapView!
   
    
    var delegate: AddAlertVCDelegate?
  

    private lazy var locationMsgsRef: DatabaseReference = Database.database().reference().child("locationReminders")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()

    }
    
  
    
 
    
    @IBAction func textFieldEditingChanged(sender: UITextField) {
        addButton.isEnabled = !radiusTextField.text!.isEmpty && !noteTextField.text!.isEmpty && !titleTextField.text!.isEmpty
    }
    
    @IBAction func onCancel(sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func onAdd(sender: AnyObject) {
        
            let coordinate = mapView.centerCoordinate
            let radius = Double(radiusTextField.text!) ?? 0
            let identifier = NSUUID().uuidString
            let note = titleTextField.text! + "[-]|[-]" + noteTextField.text!
            let eventType: EventType = (eventTypeSegmentedControl.selectedSegmentIndex == 0) ? .onEntry : .onExit
            let userId = Auth.auth().currentUser?.uid ?? ""
            let messageRef = self.locationMsgsRef.child(userId).childByAutoId()
            let messageItem:[String:Any] = [
                "lng": coordinate.longitude,
                "lat": coordinate.latitude,
                "radius": radius,
                "identifier": messageRef.key,
                "note": note,
                "eventType": eventType.rawValue,
                "sender_name": CURRENT_USER?.name ?? "Heu User",
                "sender_id": userId,
                ]

            messageRef.setValue(messageItem, withCompletionBlock: { (error, ref) in

                if error != nil {
                    self.showAlertWithTitle(title: "Failure", message: "Something went wrong with your network, please try again ")
                    
                }
                let alert = UIAlertController(title: "Success", message: "Your Message has been set successfully", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    self.delegate?.addAlertVC(controller: self, didAddCoordinate: coordinate, radius: radius, identifier: identifier, note: note, eventType: eventType, senderName: "")
                }))
                self.present(alert, animated: true, completion: nil)
            })
        
    }
    
    @IBAction private func onZoomToCurrentLocation(sender: AnyObject) {
        mapView.zoomToUserLocation()
    }
    
    // MARK: Private
    fileprivate func setupViews() {
        navigationItem.rightBarButtonItems = [addButton, zoomButton]
        addButton.isEnabled = false
    
    }
    
    
}
