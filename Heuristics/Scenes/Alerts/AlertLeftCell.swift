//
//  AlertLeftCell.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 4/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class AlertLeftCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var note: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
