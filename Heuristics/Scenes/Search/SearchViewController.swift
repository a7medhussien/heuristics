//
//  SerachViewController.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/11/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SearchViewController: HeuristicsBaseViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchContainerView: UIScrollView!
    
    lazy var tabPageController = {
        return TabPageController(viewControllers: [UIViewController]())
    } ()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadTabController()
        setupView()
    }

    
    // MARK: @IBActions
    
    @IBAction func menuBtnAct(_ sender: Any) {

    }
    
    // MARK: - Private Helpers
    fileprivate func setupView() {
        self.navigationItem.title = "Feedbacks"
//        searchBar.tintColor = .white
//        searchBar.backgroundImage = UIImage()
//        searchBar.delegate = self
    }
    
    fileprivate func loadTabController() {
        
        var controllers: [UIViewController] = [UIViewController]()
        
        
        
        
        
        // MARK: - Create PlacesViewController
        let title1 = "PLACES"
        let placesTableViewController1 = PlacesTableViewController(style: .grouped, itemInfo: IndicatorInfo(title: title1))
        controllers.append(placesTableViewController1)
        
        
        
//        // MARK: - Create PeopleViewController
//        let usersViewController = UsersViewController()
//        controllers.append(usersViewController)
        
        
        self.view.layoutIfNeeded()
        
        // MARK: - Create TabPageController
        tabPageController = TabPageController(viewControllers: controllers)
        placesTableViewController1.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        placesTableViewController1.view.frame = self.searchContainerView.bounds
        
        self.addChildViewController(placesTableViewController1)
        self.searchContainerView.addSubview(placesTableViewController1.view)
        placesTableViewController1.didMove(toParentViewController: self)
        self.view.layoutIfNeeded()
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        if tabPageController.currentIndex == 0 {
            
            guard let placeTVC = tabPageController.viewControllers[0] as? PlacesTableViewController else {
                return
            }
            placeTVC.placesFiltered  = placeTVC.places.filter({( place : GooglePlace) -> Bool in
                if searchBarIsEmpty() {
                    placeTVC.placesFiltered = placeTVC.places
                    return true
                } else {
                    return place.name.lowercased().contains(searchText.lowercased())
                }
            })
            placeTVC.tableView.reloadData()
        } else {
            
            guard let userVC = tabPageController.viewControllers[1] as? UsersViewController else {
                return
            }
            userVC.usersFiltered  = userVC.users.filter({( user : User) -> Bool in
                if searchBarIsEmpty() {
                    userVC.usersFiltered = userVC.users
                    return true
                } else {
                    return user.name!.lowercased().contains(searchText.lowercased()) || user.userName!.lowercased().contains(searchText.lowercased())
                }
            })
            userVC.tableView.reloadData()
            
        }

        
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchBar.text?.isEmpty ?? true
    }
}

extension SearchViewController: UISearchBarDelegate {
    
    
    internal func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
        filterContentForSearchText(searchBar.text!)
    }
   
}


