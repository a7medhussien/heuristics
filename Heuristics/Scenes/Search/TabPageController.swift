//
//  TabPageController.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 10/4/17.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class TabPageController: ButtonBarPagerTabStripViewController {
    
    var passedControllers: [UIViewController]
    
    init(viewControllers: [UIViewController]) {
        self.passedControllers = viewControllers
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        setupTabbarSettings()
        super.viewDidLoad()
        setupTabbarAnimation()
        
    }
    
    func setupTabbarSettings() {
        
        settings.style.buttonBarBackgroundColor = .mainAppColor
        settings.style.buttonBarItemBackgroundColor = .mainAppColor
       // settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size: 17)!
        settings.style.selectedBarHeight = 2
        
        settings.style.buttonBarHeight = 50
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 20
        settings.style.buttonBarRightContentInset = 20
        
    }
    
    func setupTabbarAnimation() {
        
        buttonBarView.selectedBar.backgroundColor = .darkGray
        buttonBarView.backgroundColor = .mainAppColor
        
        //selection animation
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .darkGray
            newCell?.label.textColor = .darkGray
            
            if animated {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    newCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    oldCell?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                })
            } else {
                newCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                oldCell?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            }
        }
    }
    
    // MARK: - PagerTabStripDataSource
    // Setup Tabs ViewControllers
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        
        return passedControllers
        
    }
    
}
