//
//  PlacesTableViewController.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/10/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Kingfisher

class PlacesTableViewController: UITableViewController {
    
    
    
    var currentUser = CURRENT_USER
    lazy var places = {
        return [GooglePlace]()
    }()
    
    var placesFiltered = {
    return [GooglePlace]()
    }()
    
    var dataLoaded = false
    
    var itemInfo = IndicatorInfo(title: "View")
    
    init(style: UITableViewStyle, itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(style: style)
    }
    
    init() {
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupRefreshControl()
        
        loadGooglePlaces()
    }
    
    
    
    // MARK: - Private Helpers
    
    
    func setupRefreshControl () {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
    }

    func refresh(refreshControl: UIRefreshControl) {

        loadGooglePlaces()
    }
    
    fileprivate func setupTableView() {

        tableView.refreshControl?.isEnabled = true
        tableView.tableFooterView = UIView()
        let placeCellNib = UINib(nibName: "PlaceCell", bundle: nil)
        tableView.register(placeCellNib, forCellReuseIdentifier: "PlaceCell")
        
        let errorMessageNib = UINib(nibName: "ErrorMessageCell", bundle: nil)
        tableView.register(errorMessageNib, forCellReuseIdentifier: "ErrorMessageCell")
        
        let loadingNib = UINib(nibName: "LoadingCell", bundle: nil)
        tableView.register(loadingNib, forCellReuseIdentifier: "LoadingCell")
    }
    
    fileprivate func loadGooglePlaces() {
        
        
        APIManager.defaultManager.getNearbyPlaces(latitude: currentUser?.lat ?? 30.100112, longitude: currentUser?.long ?? 31.317637, radius: 2000) { (places, status, msg) in
            
            if status {
                self.places = places!
                self.placesFiltered = self.places
            }
            DispatchQueue.main.async {
                
                self.dataLoaded = true
                self.tableView.reloadData()
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
}

// MARK: - Table View Funcs
extension PlacesTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if placesFiltered.isEmpty {
            return 1
        } else {
            return placesFiltered.count
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !dataLoaded {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as? LoadingCell
            let message = "Loading ... "
            cell?.display(loadingMessage: message)
            return cell!
            
        }
        
        if placesFiltered.isEmpty {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorMessageCell", for: indexPath) as? ErrorMessageCell
            let message = "No nearby places"
            cell?.display(message: message)
            return cell!
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell", for: indexPath) as? PlaceCell
            let currentPlace = placesFiltered[indexPath.row]

            if let photoRef = currentPlace.photoReference {
                let urlString = APIManager.defaultManager.getPhotoLinkFromReference(photoRef)
                let imageURL = URL(string: urlString)
                cell?.placeImage.kf.setImage(with: imageURL)
            } else {
                cell?.placeImage.image = UIImage(named: "defaultPlace")
            }
            
            cell?.placeName.text! = currentPlace.name
            cell?.placeSubName.text! = currentPlace.placeType
            return cell!
            
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? PlaceCell else {
            return
        }
        let currentPlace = self.placesFiltered[indexPath.row]
        let placeVC = PlaceViewController(nibName: "PlaceViewController", bundle: nil)
        placeVC.placeID = currentPlace.placeID
        placeVC.loadedImage = cell.placeImage.image
        placeVC.placeName = handleLongNameTitle(name: cell.placeName.text!)
        
        
        if let parent = self.parent as? SearchViewController {
            parent.pushVC(placeVC)
            //present(placeVC, animated: false, completion: nil)
            //parent.navigationController?.pushViewController(placeVC, animated: false)
            //self.present(placeVC, animated: true, completion: nil)
            //parent.
        }
    }
    
    fileprivate func handleLongNameTitle(name:String) -> String {
        let titleArr = name.split(" ")
        if titleArr.count >= 2 {
            let subTitleArr = titleArr[..<2]
            return subTitleArr[0] + " " + subTitleArr[1]
        }else {
            return titleArr[0]
        }
    }

    
}

// MARK: - IndicatorInfoProvider
extension PlacesTableViewController: IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

