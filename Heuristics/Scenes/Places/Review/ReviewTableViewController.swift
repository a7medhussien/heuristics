//
//  ReviewTableViewController.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/12/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Kingfisher

class ReviewTableViewController: UITableViewController {
    
    var reviews: [GoogleReview]
    var dataLoaded = false
    
    var itemInfo = IndicatorInfo(title: "View")
    
    init(style: UITableViewStyle, itemInfo: IndicatorInfo, reviews: [GoogleReview]) {
        self.itemInfo = itemInfo
        self.reviews = reviews
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        registerTableViewCells()
        
    }
    
    // MARK: - Private Helpers
    fileprivate func setupTableView() {
        //Table properties
        let backgorundView = UIView()
        backgorundView.backgroundColor = .mainAppColor
        tableView.tableFooterView = backgorundView
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    fileprivate func registerTableViewCells() {
        
        tableView.tableFooterView = UIView()
        let placeCellNib = UINib(nibName: "ReviewCell", bundle: nil)
        tableView.register(placeCellNib, forCellReuseIdentifier: "ReviewCell")
        
        let errorMessageNib = UINib(nibName: "ErrorMessageCell", bundle: nil)
        tableView.register(errorMessageNib, forCellReuseIdentifier: "ErrorMessageCell")
        
        let loadingNib = UINib(nibName: "LoadingCell", bundle: nil)
        tableView.register(loadingNib, forCellReuseIdentifier: "LoadingCell")
    }
}


// MARK: UITableViewDelegate
extension ReviewTableViewController {
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if reviews.isEmpty {
            return 1
        } else {
            return reviews.count
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return UITableViewAutomaticDimension
        
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

// MARK: UITableViewDataSource
extension ReviewTableViewController {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if reviews.isEmpty {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorMessageCell", for: indexPath) as? ErrorMessageCell
            let message = "No reviews about this place"
            cell?.display(message: message)
            return cell!
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as? ReviewCell
            let currentPlace = reviews[indexPath.row]
            
            cell?.authorName.text! = currentPlace.authorName
            cell?.date.text! = currentPlace.time
            cell?.reviewContent.text! = currentPlace.reviewContent
            
            cell?.rating.rating = currentPlace.rating
            cell?.rating.settings.updateOnTouch = false // disable editing on the rate
            
            let imgURL = URL(string: currentPlace.profilePhotoURL)
            cell?.profilePhoto.kf.setImage(with: imgURL)
            
            return cell!
            
        }
    }
    
}


// MARK: - IndicatorInfoProvider
extension ReviewTableViewController: IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

