//
//  AddReviewViewController.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import Cosmos
import DateToolsSwift

protocol AddReviewViewControllerProtocol {
    func submitReview(withReview review: HeuReview)
}

class AddReviewViewController: UIViewController {

    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingContainerView: UIView!
    
    @IBOutlet weak var reviewTextView: UITextView!
    
    var delegate: AddReviewViewControllerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backGroundView.alpha = 0.0
        self.ratingContainerView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3) { [unowned self] in
            self.backGroundView.alpha = 0.6
            self.ratingContainerView.transform = CGAffineTransform.identity
        }
    }
    
    // MARK: - @IBActions
    @IBAction func closeBtnAct(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.backGroundView.alpha = 0.0
            self.ratingContainerView.transform = CGAffineTransform(translationX: 0,
                                                                    y: self.view.frame.height)
        }) {[unowned self] (_)  in
            self.dismiss(animated: false, completion: nil)
        }
    }
    @IBAction func submitBtnAct(_ sender: Any) {

        
        if reviewTextView.text!.isEmpty {
            self.showErrorAlert(withMessage: "Please enter your review")
            return
        }
        let reviewDic = [
            "review" : reviewTextView.text!,
            "rating" : ratingView.rating,
            "placeID" : "",
            "authorName" :  "",
            "time" : "\(Date())"
        
        ] as [String : Any]
        
        let heuReview = HeuReview(dictionary: reviewDic)
        self.delegate?.submitReview(withReview: heuReview)
        
        self.dismiss(animated: true, completion: nil)
       
        
    }

}
