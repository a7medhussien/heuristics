//
//  ReviewCell.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/12/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import Cosmos

class ReviewCell: UITableViewCell {

    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var reviewContent: UILabel!
    @IBOutlet weak var date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
