//
//  HeuReviewTableViewController.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/20/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class HeuReviewTableViewController: UITableViewController {
        
  
    var dataLoaded = false
    
    lazy var heuReviews = {
        return [HeuReview]()
    }()
    
    // MARK: - Data Passed
    var placeID: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        registerTableViewCells()
        loadHeuReviews()
    }
    
    // MARK: - Private Helpers
    fileprivate func setupTableView() {
        //Table properties
        let backgorundView = UIView()
        backgorundView.backgroundColor = .mainAppColor
        tableView.tableFooterView = backgorundView
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    fileprivate func registerTableViewCells() {
        
        tableView.tableFooterView = UIView()
        let placeCellNib = UINib(nibName: "ReviewCell", bundle: nil)
        tableView.register(placeCellNib, forCellReuseIdentifier: "ReviewCell")
        
        let errorMessageNib = UINib(nibName: "ErrorMessageCell", bundle: nil)
        tableView.register(errorMessageNib, forCellReuseIdentifier: "ErrorMessageCell")
        
        let loadingNib = UINib(nibName: "LoadingCell", bundle: nil)
        tableView.register(loadingNib, forCellReuseIdentifier: "LoadingCell")
    }
    
    fileprivate func loadHeuReviews() {
        
        APIManager.defaultManager.getReviews(withPlaceID: placeID) { (reviews, status, msg) in
            DispatchQueue.main.async {
                
                if status {
                    guard let reviews = reviews else {
                        self.heuReviews = [HeuReview(dictionary: [:])]
                        self.tableView.reloadData()
                        return
                    }
                    self.heuReviews = reviews
                    
                }
                self.tableView.reloadData()
            }
            
        }
    }
}


// MARK: UITableViewDelegate
extension HeuReviewTableViewController {
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if heuReviews.isEmpty {
            return 1
        } else {
            return heuReviews.count
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return UITableViewAutomaticDimension
        
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

// MARK: UITableViewDataSource
extension HeuReviewTableViewController {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if heuReviews.isEmpty {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ErrorMessageCell", for: indexPath) as? ErrorMessageCell
            let message = "No reviews about this place"
            cell?.display(message: message)
            return cell!
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as? ReviewCell
            let currentPlace = heuReviews[indexPath.row]
            
            cell?.authorName.text! = currentPlace.authorName
            cell?.date.text! = currentPlace.time
            cell?.reviewContent.text! = currentPlace.reviewContent
            
            cell?.rating.rating = currentPlace.rating
            cell?.rating.settings.updateOnTouch = false // disable editing on the rate
            
            return cell!
            
        }
    }
    
}


// MARK: - IndicatorInfoProvider
extension HeuReviewTableViewController: IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "Heu Reviews"
    }
}

