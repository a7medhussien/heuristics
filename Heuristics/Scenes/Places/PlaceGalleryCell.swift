//
//  PlaceGalleryCell.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 3/15/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class PlaceGalleryCell: UICollectionViewCell {

    @IBOutlet weak var placeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
