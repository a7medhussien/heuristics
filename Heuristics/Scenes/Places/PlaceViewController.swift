//
//  PlaceViewController.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 2/10/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import Cosmos
import MapKit
import CoreLocation
import XLPagerTabStrip

class PlaceViewController: UIViewController {
    
    
    var currentUser = CURRENT_USER
    

    @IBOutlet weak var placeAddress: UILabel!
    @IBOutlet weak var placePhoneNumber: UILabel!
    @IBOutlet weak var placeRatingView: CosmosView!
    @IBOutlet weak var placesGalleryCollectionView: UICollectionView!
    @IBOutlet weak var placeDetailsRestViews: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    // MARK: Container View
    @IBOutlet weak var placeContainerView: UIScrollView!
    
    
    lazy var tabPageController = {
        return TabPageController(viewControllers: [UIViewController]())
    } ()
    
    
    var placeDetails: GooglePlaceDetails?
    
    // MARK: Data Passed From Prev View
    var placeID: String!
    var loadedImage: UIImage!
    var placeName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupData()
        setupGalleryCollectionView()
        loadPlaceDetails()
    }
    
    // MARK: - @IBActions
   
    @IBAction func openMapBtnAct(_ sender: Any) {
        
        let placeLat = self.placeDetails?.coordinate.latitude ?? 30
        let placeLong = self.placeDetails?.coordinate.longitude ?? 31
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(placeLat),\(placeLong)&directionsmode=driving")! as URL)
            
        } else {
            NSLog("Can't use comgooglemaps://");
        }

    }
    
    // MARK: - Private Helpers
    fileprivate func setupViews() {
        
        self.navigationItem.title = self.placeName
        let addFeedbackBtn = UIBarButtonItem(barButtonSystemItem: .add,
                                           target: self,
                                           action: #selector(addFeedbackPressed(_:)))
        self.navigationItem.rightBarButtonItem = addFeedbackBtn
    }
    func addFeedbackPressed(_ sender: UIButton) {
        let addReviewVC = AddReviewViewController(nibName: "AddReviewViewController", bundle: nil)
        addReviewVC.modalPresentationStyle = .overFullScreen
        addReviewVC.delegate = self
        self.present(addReviewVC, animated: false, completion: nil)
        
    }
    
    fileprivate func setupGalleryCollectionView() {
        registerGalleryCollectionCell()
        handleGalleryCollectionLayout()
    }
    
    fileprivate func registerGalleryCollectionCell() {
        
        let nib = UINib(nibName: "PlaceGalleryCell", bundle: nil)
        self.placesGalleryCollectionView.register(nib, forCellWithReuseIdentifier: "PlaceGalleryCell")
    }
    
    fileprivate func handleGalleryCollectionLayout () {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: self.view.getMainScreenWidth(), height: 185)
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        self.placesGalleryCollectionView?.collectionViewLayout = layout
    }
    
    fileprivate func loadPlaceDetails() {
        // load place details with place id
        if let placeID = placeID {
            APIManager.defaultManager.getPlaceDetails(placeID: placeID) { (placeDetails, status, msg) in
                if status {
                    if let placeDetails = placeDetails {
                        
                        self.placeDetails = placeDetails
                        self.setupData()
                        self.loadTabController(withReviews: placeDetails.reviews)
                        self.pageControl.numberOfPages = self.placeDetails?.photosURLs.count ?? 1
                        self.placesGalleryCollectionView.reloadData()
                        
                    }
                    
                } else {
                    
                }

            }
        }else {
            
        }
    }
    
    fileprivate func setupData() {
   
        self.placeAddress.text! = placeDetails?.address ?? ""
        self.placePhoneNumber.text! = placeDetails?.phoneNumber ?? ""
        self.placeRatingView.rating = placeDetails?.rating ?? 2
        
    }
    

    
    
    
    
    fileprivate func loadTabController(withReviews reviews: [GoogleReview]) {
        
        var controllers: [UIViewController] = [UIViewController]()
        
        
        
        
        // MARK: - Create Google Reviews
        let title = "Google Reviews"
        let reviewsTableVC = ReviewTableViewController(
            style: .grouped,
            itemInfo: IndicatorInfo(title: title),
            reviews: reviews
        )
        controllers.append(reviewsTableVC)
        
        // MARK: - Create Heu Reviews
        let heuReviewTableViewController = HeuReviewTableViewController()
        heuReviewTableViewController.placeID = self.placeID
        controllers.append(heuReviewTableViewController)
        
        /*
        // MARK: - Create Circle Reviews
        let heuCircleReviewTVC = HeuReviewTableViewController()
        heuReviewTableViewController.placeID = self.placeID
        controllers.append(heuCircleReviewTVC)
        */
        tabPageController = TabPageController(viewControllers: controllers)
        tabPageController.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        tabPageController.view.frame = self.placeContainerView.bounds
        
        self.addChildViewController(tabPageController)
        self.placeContainerView.addSubview(tabPageController.view)
        tabPageController.didMove(toParentViewController: self)
    }

}

extension PlaceViewController: AddReviewViewControllerProtocol {
    
    func submitReview(withReview review: HeuReview) {
        
        review.placeID = self.placeID
        APIManager.defaultManager.addReview(withReview: review)
        if let heuReviewTVC = tabPageController.viewControllers[1] as? HeuReviewTableViewController {
            DispatchQueue.main.async {
                heuReviewTVC.tableView.reloadData()
            }
        }
    }

}

// MARK: - UICollectionViewDataSource
extension PlaceViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let photosCount = self.placeDetails?.photosURLs.count ?? 0
        
        return photosCount == 0 ? 1:photosCount
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! PlaceGalleryCell
        self.displayImageFullScreen(with: cell.placeImage.image ?? UIImage())
      
    }
    fileprivate func displayImageFullScreen(with image:UIImage) {

        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc fileprivate func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }

}

// MARK: - UICollectionViewDelegate
extension PlaceViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
    let cell = self.placesGalleryCollectionView.dequeueReusableCell(withReuseIdentifier: "PlaceGalleryCell", for: indexPath) as? PlaceGalleryCell
        
        if indexPath.row == 0 {
            cell?.placeImage.image = loadedImage
        }else {
            let currentPhoto = self.placeDetails?.photosURLs[indexPath.row] ?? ""
            let urlString = APIManager.defaultManager.getPhotoLinkFromReference(currentPhoto)
            let imageURL = URL(string: urlString)
            cell?.placeImage.kf.setImage(with: imageURL)
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }
    
}

