//
//  Memory.swift
//  Heuristics
//
//  Created by Ahmed Zaghloul on 2/11/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import MapKit
import Firebase

protocol AddGeotificationsViewControllerDelegate {
    func addGeotificationViewController(controller: AddGeotificationViewController, didAddCoordinate coordinate: CLLocationCoordinate2D,
                                        radius: Double, identifier: String, note: String, eventType: EventType,senderName:String)
}
enum GeotificationType {
    case alert
    case message
}

class AddGeotificationViewController: UIViewController {
    
    
  
    @IBOutlet weak var radiusTextField: UITextField!
    @IBOutlet weak var titleDartTextField: UITextField!
    @IBOutlet weak var contentDartTextField: UITextField!
   
    @IBOutlet weak var selectUserView: UIView!
    @IBOutlet weak var onExitFlagImageView: UIImageView!
    @IBOutlet weak var onEntryFlagImageView: UIImageView!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var chooseUserLabel: UILabel!
    @IBOutlet weak var locationTextField: UITextField!
    
    var delegate: AddGeotificationsViewControllerDelegate?
    var receiver: User?
    var onEntry = 0
    var location: CLLocationCoordinate2D?
    private lazy var locationMsgsRef: DatabaseReference = Database.database().reference().child("location_messages")

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        loadCurrentUser()
    }
    
    @IBAction func onEntryGestureTapped(_ sender: UITapGestureRecognizer) {
        self.onEntry = 1
        self.onEntryFlagImageView.image = #imageLiteral(resourceName: "tick")
        self.onExitFlagImageView.image = UIImage()
    }

    @IBAction func onExitGestureTapped(_ sender: UITapGestureRecognizer) {
        self.onEntry = 0
        self.onEntryFlagImageView.image = UIImage()
        self.onExitFlagImageView.image = #imageLiteral(resourceName: "tick")
    }
    
    @IBAction func onLocationTapped(_ sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Darts", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func sendButtonAct(_ sender: Any) {
        
        if titleDartTextField.text! == "" || contentDartTextField.text! == "" || radiusTextField.text! == "" {
            self.showAlertWithTitle(title: "Failure", message: "Please enter empty fields")
            return
        }
        guard let location = self.location else {
            self.showAlertWithTitle(title: "Failure", message: "Please choose a dart location")
            return
        }
        if receiver == nil {
            self.showAlertWithTitle(title: "Missing", message: "Can not send message to empty user, please choose specific user")
            return
            //            let coordinate = mapView.centerCoordinate
            //            let radius = Double(radiusTextField.text!) ?? 0
            //            let identifier = NSUUID().uuidString
            //            let note = noteTextField.text!
            //            let eventType: EventType = (eventTypeSegmentedControl.selectedSegmentIndex == 0) ? .onEntry : .onExit
            //            delegate?.addGeotificationViewController(controller: self, didAddCoordinate: coordinate, radius: radius, identifier: identifier, note: note, eventType: eventType, senderName: "")

        }else{
            let eventType: EventType = (self.onEntry == 0) ? .onEntry : .onExit

            let messageRef = self.locationMsgsRef.child((receiver?.userID!)!).childByAutoId()
            let note = titleDartTextField.text! + "[-]|[-]" + contentDartTextField.text!
            let messageItem:[String:Any] = [
                "lng": location.longitude ,
                "lat": location.latitude ,
                "radius": Double(radiusTextField.text!) ?? 0,
                "identifier": messageRef.key,
                "note": note,
                "eventType": eventType.rawValue,
                "sender_name": CURRENT_USER?.name ?? "Fail Current User",
                "sender_id":(Auth.auth().currentUser?.uid)!,
                ]

            messageRef.setValue(messageItem, withCompletionBlock: { (error, ref) in
                if error == nil {
                    let senderId = self.receiver?.userID ?? ""
                    let senderTitle =  "\(CURRENT_USER?.name ?? "Heu User") send you a dart"
                    let contenMessage = "To open the message please go to \(self.locationTextField.text!)."
            
                    let messageBody = MessageBodyNotification(userId: senderId, contentMessage: contenMessage,senderName: senderTitle)
                        APIManager.defaultManager.postMessageWithBody(messageBody: messageBody)
                    
                    let alert = UIAlertController(title: "Success", message: "Your dart leaved successfully", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
    }
    @IBAction func selectUserBtnAct(_ sender: Any) {
        let usersVC = UsersListViewController()
        usersVC.listType = .following
        usersVC.dataSource = self
        self.pushVC(usersVC)
    }
    @IBAction func cancelBarButtonAct(_ sender: Any) {
        self.dismissVC(completion: nil)
    }
    //    @IBAction func textFieldEditingChanged(sender: UITextField) {
//        .isEnabled = !radiusTextField.text!.isEmpty && !noteTextField.text!.isEmpty
//    }
    
 
    func getCurrentGeoLocation(lat: Double, long: Double) -> String{
        let location = CLLocation(latitude: lat,longitude: long)
        var locationStr = ""
        CLGeocoder().reverseGeocodeLocation(location, completionHandler:
            {(placemarks, error) in
                if (error == nil) {
                    return
                }
                if let placemarks = placemarks {
                    if placemarks.count > 0 {
                        
                        locationStr = "\(placemarks)"
                    }
                }
        })
        return locationStr == "" ? "":locationStr
    }
    
    // MARK: Private
    fileprivate func loadCurrentUser() {
        if let uid = Auth.auth().currentUser?.uid, CURRENT_USER == nil  {
            APIManager.defaultManager.getUser(uid: uid, completionHandler: {(user, success, message) in
                if success && user != nil {
                    CURRENT_USER =  user
                }
            })
        }
    }
    
    
    fileprivate func setupViews() {
        
    }
}

extension AddGeotificationViewController: UserListDataSoruce {
    
    
    func didselect(User user: User?) {
        guard let user = user else {
            return
        }
        self.chooseUserLabel.text = "Dart To: " + (user.name ?? "")
        self.chooseUserLabel.textColor = .black
        self.receiver = user
    }
}

extension AddGeotificationViewController: LocationPikerDelegate{
    func LocationPiker(didPicked location: CLLocationCoordinate2D, locationDescription: String, placemark: CLPlacemark?) {
        self.location = location
        //        administrativeArea = (placemark?.locality != nil) ? placemark?.locality! : " "
        //         city = (placemark?.administrativeArea != nil) ? placemark?.administrativeArea! : " "
        //         country = (placemark?.country != nil) ? placemark?.country! : " "
        
        locationTextField.text = locationDescription
        
    }
}


