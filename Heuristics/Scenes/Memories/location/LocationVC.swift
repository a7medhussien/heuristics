//
//  locationVC.swift
//  HeuristicsOffers
//
//  Created by Magdy Zamel on 8/2/17.
//  Copyright © 2017 Fxlab. All rights reserved.
//

import UIKit
import MapKit

protocol HandleMapSearch: class {
    func dropPinZoomIn(_ placemark:MKPlacemark)
}
protocol LocationPikerDelegate: class {
    func LocationPiker(didPicked location: CLLocationCoordinate2D,locationDescription:String ,placemark : CLPlacemark?)
}

class LocationVC: HeuristicsBaseViewController {

    @IBOutlet weak var mapView: MKMapView!

    var selectedPlaceMark: MKPlacemark?
    var resultSearchController: UISearchController!

    let locationManager = CLLocationManager()
    weak var delegate:LocationPikerDelegate?

    var searchBar:UISearchBar!
    let annotation = MKPointAnnotation()
    var placeMark:CLPlacemark!
     let reusePinId = "reusePinId"

     let editCustomLoactionSegue = "editCustomLoactionSegue"

    override func setup() {
        super.setup()
        UiHelpers.showLoader()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()

        mapView.showsUserLocation = true

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)

    }

    override func setupViews() {
        super.setupViews()
        self.navigationItem.setHidesBackButton(true, animated: true)
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable

        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController.searchResultsUpdater = locationSearchTable
         searchBar = resultSearchController!.searchBar
         searchBar.placeholder = "Search for places ..."
        
     searchBar.frame = CGRect(x: 22, y: 0, w: UIScreen.main.bounds.width-64, h: 34)
     searchBar.tintColor  = UIColor.white
        searchBar.delegate = self

        let backBarButton = UIButton(type: .system)
        backBarButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        backBarButton.frame = CGRect(x: 0, y: 0, w: 8, h: 44)
        backBarButton.addTapGesture { recognizer in
            self.navigationController?.popViewController(animated: true)
        }
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backBarButton) ,UIBarButtonItem(customView: resultSearchController!.searchBar)]

        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self


    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UiHelpers.checkingLocationServicesEnabledAlertOn(viewController: self)
        if !CLLocationManager.locationServicesEnabled() {
            let alertController = UIAlertController (title: "Failure", message: "Please enable location services from Setting > LocationServices.  ", preferredStyle: .alert)

            let settingsAction = UIAlertAction(title: "Location service", style: .default) { (_) -> Void in

                UIApplication.shared.openURL(URL(string : "\(UIApplicationOpenSettingsURLString)path=LOCATION_SERVICES")!)
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Later", style: .default, handler: nil)
            alertController.addAction(cancelAction)
            presentVC(alertController)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //    self.navigationController?.setNavigationBarHidden(true, animated: true)
        unSubscribeToKeyboardNotifications()
    }

    func unSubscribeToKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
    }

    


    @IBAction func centerMapOnUserButtonClicked() {

        if !CLLocationManager.locationServicesEnabled() {
            let alertController = UIAlertController (title: "Failure", message: "Please enable location services from Setting > LocationServices.  ", preferredStyle: .alert)

            let settingsAction = UIAlertAction(title: "Location service", style: .default) { (_) -> Void in

               UIApplication.shared.open((URL(string : "\(UIApplicationOpenSettingsURLString)path=LOCATION_SERVICES")!), options: [:], completionHandler: nil)


            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Later", style: .default, handler: nil)
            alertController.addAction(cancelAction)
            presentVC(alertController)
        }else{
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                break;
            case .authorizedAlways, .authorizedWhenInUse:
                let span = MKCoordinateSpanMake(0.7, 0.7)
                if locationManager.location == nil{
                    locationManager.requestLocation()
                    centerMapOnUserButtonClicked()
                }
                let region = MKCoordinateRegion(center: locationManager.location!.coordinate, span: span)
                mapView.setRegion(region, animated: true)

            }
        }

    }

    
     @objc func selectPin(){
        guard let selectedPlaceMark = selectedPlaceMark else {
            delegate?.LocationPiker(didPicked: locationManager.location!.coordinate,locationDescription:annotation.subtitle!, placemark: nil)
            self.popVC()
            return
        }
        self.delegate?.LocationPiker(didPicked: selectedPlaceMark.coordinate,locationDescription:annotation.subtitle!, placemark: selectedPlaceMark)
        self.popVC()

    }


    @IBAction func editLocationButtonTapped() {
        performSegue(withIdentifier: editCustomLoactionSegue, sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segueId = segue.identifier , segueId == editCustomLoactionSegue{
            let navigationController = segue.destination as! UINavigationController
            let editCustomLoactionVC = navigationController.topViewController as! EditCustomLoactionVC
            editCustomLoactionVC.delegate = self
            editCustomLoactionVC.coordinate = self.annotation.coordinate

        }
    }


}

extension LocationVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }


        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            UiHelpers.hideLoader()
            self.locationManager.stopUpdatingLocation()
            if let placemarks = placemarks, placemarks.count > 0 {
                self.placeMark = placemarks[0]
                let placemark = placemarks[0]
                self.annotation.coordinate = location.coordinate
                self.annotation.title = placemark.name

                self.selectedPlaceMark = MKPlacemark(placemark: placemark)

                let administrativeArea = (placemark.locality != nil) ? placemark.locality! : " "
                let city = (placemark.administrativeArea != nil) ? placemark.administrativeArea! : " "
                let country = (placemark.country != nil) ? placemark.country! : " "

                let loactionDescription = "\(country) \(city) \(administrativeArea)"
                self.annotation.subtitle = loactionDescription


                self.mapView.addAnnotation(self.annotation)
                let span = MKCoordinateSpanMake(0.7, 0.7)
                let region = MKCoordinateRegionMake(location.coordinate, span)
                self.mapView.setRegion(region, animated: true)
                self.mapView.selectAnnotation(self.annotation, animated: true)

            } else {
                let span = MKCoordinateSpanMake(0.7, 0.7)
                let region = MKCoordinateRegion(center: location.coordinate, span: span)
                let myAnnotation: MKPointAnnotation = MKPointAnnotation()
                myAnnotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
                myAnnotation.title = "Current location"
                self.mapView.addAnnotation(myAnnotation)

                self.mapView.addAnnotation(self.annotation)
                self.mapView.setRegion(region, animated: true)
                self.mapView.selectAnnotation(self.annotation, animated: true)

            }
        })



    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        UiHelpers.hideLoader()
        print("error:: \(error.localizedDescription)")
    }

}

extension LocationVC: HandleMapSearch {
    
    func dropPinZoomIn(_ placemark: MKPlacemark){


        // cache the pin
        selectedPlaceMark = placemark
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        
        let administrativeArea = (placemark.locality != nil) ? placemark.locality! : " "
        let city = (placemark.administrativeArea != nil) ? placemark.administrativeArea! : " "
        let country = (placemark.country != nil) ? placemark.country! : " "

        let loactionDescription = "\(country) \(city) \(administrativeArea)"
        annotation.subtitle = loactionDescription


        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpanMake(0.7, 0.7)
        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        mapView.setRegion(region, animated: true)
        mapView.selectAnnotation(annotation, animated: true)

    }

}

extension LocationVC : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{

        guard !(annotation is MKUserLocation) else { return nil }
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reusePinId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reusePinId)
        }

        pinView?.pinTintColor = UIColor.mainAppColor
        pinView?.canShowCallout = true
        let smallSquare = CGSize(width: 30, height: 30)
        let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: smallSquare))
        button.setBackgroundImage(#imageLiteral(resourceName: "selectLocation"), for: UIControlState())
        button.addTarget(self, action: #selector(LocationVC.selectPin), for: .touchUpInside)
        pinView?.leftCalloutAccessoryView = button

        return pinView
    }

     @objc func keyboardWillShow(sender: NSNotification) {
//        searchBar.frame = CGRect(x: 22, y: 0, w: UIScreen.main.bounds.width-64, h: 34)

    }

}
extension LocationVC:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        searchBar.frame = CGRect(x: 22, y: 0, w: UIScreen.main.bounds.width-64, h: 34)
    }

}

extension LocationVC:EditCustomLoactionDelegate{

    func editCustomLoaction(didCanceled controller: EditCustomLoactionVC) {
        controller.dismissVC(completion: nil)
    }

    func editCustomLoaction(didPicked location: CLLocationCoordinate2D, controller: EditCustomLoactionVC) {
        let location = CLLocation(latitude: location.latitude, longitude: location.longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
            self.locationManager.stopUpdatingLocation()
            if let placemarks = placemarks, placemarks.count > 0 {
                let placemark = placemarks[0]
                self.annotation.coordinate = location.coordinate
                self.annotation.title = placemark.name

                let administrativeArea = (placemark.locality != nil) ? placemark.locality! : " "
                let city = (placemark.administrativeArea != nil) ? placemark.administrativeArea! : " "
                let country = (placemark.country != nil) ? placemark.country! : " "

                let loactionDescription = "\(country) \(city) \(administrativeArea)"
                self.annotation.subtitle = loactionDescription

                self.selectedPlaceMark = MKPlacemark(placemark: placemark)

                self.mapView.addAnnotation(self.annotation)
                let span = MKCoordinateSpanMake(0.7, 0.7)
                let region = MKCoordinateRegionMake(location.coordinate, span)
                self.mapView.setRegion(region, animated: true)
                self.mapView.selectAnnotation(self.annotation, animated: true)

            } else {

                let span = MKCoordinateSpanMake(0.7, 0.7)
                let region = MKCoordinateRegion(center: location.coordinate, span: span)
                let myAnnotation: MKPointAnnotation = MKPointAnnotation()
                myAnnotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
                myAnnotation.title = "Current location"
                self.mapView.addAnnotation(myAnnotation)
                self.mapView.addAnnotation(self.annotation)
                self.mapView.setRegion(region, animated: true)
                self.mapView.selectAnnotation(self.annotation, animated: true)
                if #available(iOS 10.0, *) {
                    self.selectedPlaceMark = MKPlacemark(coordinate: location.coordinate)
                }

            }
        })
        controller.dismissVC(completion: nil)
    }

}



