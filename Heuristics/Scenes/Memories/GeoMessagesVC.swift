//
//  GeoMessagesVC.swift
//  Heuristics
//
//  Created by Ahmed Zaghloul on 2/20/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher
import CoreLocation
import MapKit


var dartsLocalNotificationIds = [String: String]()

class GeoMessagesVC: BaseViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var tableView:UITableView!
    var locationManager = CLLocationManager()
    private lazy var locMsgsRef: DatabaseReference = Database.database().reference().child("location_messages")
    
    weak var delegate :MessageUsersDelegate?
    var sentMessages :[Geotification] = []
    var receviedMessages :[Geotification] = []
   
    
    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        getSentMessages()
        getReceivedMessages()
        locationManager.requestAlwaysAuthorization()
    }

    // MARK: - Actions
    @IBAction func addNewDartBarButtonAction(_ sender: Any) {
    }
    @IBAction func segmentBtnAct(_ sender: UISegmentedControl) {
        self.tableView.reloadData()
        self.tableView.scrollsToTop = true
    }
    
    // MARK: Firebase related methods
    
    fileprivate func getReceivedMessages() {
        let userId = Auth.auth().currentUser?.uid ?? ""
        self.activityIndicator.startAnimating()
        APIManager.defaultManager.getUserReceivedMessages(
        withUserId: userId) { (messages, status, msg) in
            self.activityIndicator.stopAnimating()
            if status {
                if let messages = messages {
                    self.receviedMessages = messages
                    self.tableView.reloadData()
                    for message in self.receviedMessages {
                        
                        self.startMonitoring(geotification: message)
                    }
                    
                    self.saveAllGeotificationMessages()
                }
            }
        }
    }
    
    private func getSentMessages() {
        // Use the observe method to listen for new
        // channels being written to the Firebase DB
        self.activityIndicator.startAnimating()
        APIManager.defaultManager.getUserSentMessages(
        withUserId: Auth.auth().currentUser?.uid ?? "",
        completion: { (messages, status, msg) in
            self.activityIndicator.stopAnimating()
            if status {
                if let messages = messages {
                    
                    self.sentMessages = messages
                    self.tableView.reloadData()
                    
                }
            }
        })
//        locMsgsRef.child((Auth.auth().currentUser?.uid)!).observe(.value, with: { (snapshot) -> Void in // 1
//
//            let userData = snapshot as DataSnapshot
//
//            if let channelData = userData.value as? NSDictionary {// 2
//
//
//                let type = EventType(rawValue: channelData.getValueForKey(key: "eventType", callback: "On Entry"))!
//                let iden = channelData.getValueForKey(key: "identifier", callback: "")
//                let lat = channelData.getValueForKey(key: "lat", callback: 0.0)
//                let lng = channelData.getValueForKey(key: "lng", callback: 0.0)
//                let co = CLLocationCoordinate2D(latitude: lat, longitude: lng)
//                let nt = channelData.getValueForKey(key: "note", callback: "")
//                let rd = channelData.getValueForKey(key: "radius", callback: 100.0)
//                let sender = channelData.getValueForKey(key: "sender_name", callback: "")
//
//                let geo = Geotification(coordinate: co, radius:                     min(rd, (self.locationManager.maximumRegionMonitoringDistance)), identifier: iden, note: nt, eventType: type,senderName:sender)
//                self.geoMessages.append(geo)
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                    self.activityIndicator.stopAnimating()
//                    self.startMonitoring(geotification: geo)
//                    self.saveAllGeotificationMessages()
//                }
//            }else {
//
//
//                DispatchQueue.main.async {
//                    self.errorView.isHidden = false
//                }
//            }
//        })
//
    }
    
    func region(withGeotification geotification: Geotification) -> CLCircularRegion {
        // 1
        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
        // 2
        region.notifyOnEntry = (geotification.eventType == .onEntry)
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }
    
    func startMonitoring(geotification: Geotification) {
        // 1
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlertWithTitle(title:"Error", message: "Geofencing is not supported on this device!")
            return
        }
        // 2
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            showAlertWithTitle(title:"Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
        }
        // 3
        let region = self.region(withGeotification: geotification)
        print(region.identifier)
        // 4
        locationManager.startMonitoring(for: region)
    }
    
    func stopMonitoring(geotification: Geotification) {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geotification.identifier else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
    }
    
    func saveAllGeotificationMessages() {
        var items: [Data] = []
        for geotification in receviedMessages {
            let item = NSKeyedArchiver.archivedData(withRootObject: geotification)
            items.append(item)
        }
        UserDefaults.standard.set(items, forKey: PreferencesKeys.receivedMessages)
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if segment.selectedSegmentIndex == 1 {
            let messageLocation = receviedMessages[indexPath.row].coordinate
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(messageLocation.latitude, messageLocation.longitude)
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = receviedMessages[indexPath.row].senderName
            mapItem.openInMaps(launchOptions: options)
        }
   
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return segment.selectedSegmentIndex == 0 ? sentMessages.count:receviedMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ConversationCell
        
        
        
        if segment.selectedSegmentIndex == 0 {
            
            let messageSentArr = sentMessages[indexPath.row].note.split("[-]|[-]")
            var messageSent = sentMessages[indexPath.row].note
            if messageSentArr.count >= 2 {
                messageSent = messageSentArr[1]
            }
            cell.titleLabel.text = sentMessages[indexPath.row].senderName
            cell.subtitleLabel.text = "Message: \n\(messageSent)"
            cell.timeLabel.text = "Radius: \(sentMessages[indexPath.row].radius)m"
            cell.dartSmallImage.image = #imageLiteral(resourceName: "dartSent")
            cell.navigationImage.image = UIImage()
        }else {
            
            let messageReceivedLocation = receviedMessages[indexPath.row].coordinate
            let messageReceivedArr = receviedMessages[indexPath.row].note.split("[-]|[-]")
            var messageReceived = receviedMessages[indexPath.row].note
            if messageReceivedArr.count >= 2 {
                messageReceived = messageReceivedArr[1]
            }
            cell.titleLabel.text = receviedMessages[indexPath.row].senderName
            cell.timeLabel.text = "Area: \(receviedMessages[indexPath.row].radius)m"
            cell.dartSmallImage.image = #imageLiteral(resourceName: "dartReceived")
            cell.subtitleLabel.text = "Message: \nloading ..."
            cell.navigationImage.image = #imageLiteral(resourceName: "navigation")
            if self.isMessageNearby(withCoordinate: messageReceivedLocation.longitude, lat: messageReceivedLocation.latitude) {
                // Show an alert if application is active
                let title = "\(receviedMessages[indexPath.row].senderName) send you dart"
                let message = messageReceived
                
                
                if let value = dartsLocalNotificationIds["\(indexPath.row)"] , value == "true" {
                   
                }else {
                    let notification = UILocalNotification()
                    
                    notification.alertTitle = title
                    notification.alertBody = message
                    notification.soundName = "Default"
                    UIApplication.shared.presentLocalNotificationNow(notification)
                    dartsLocalNotificationIds["\(indexPath.row)"] = "true"
                }
                
               // }
                cell.subtitleLabel.text = message
            }else {
                
                self.getCurrentGeoLocation(
                    lat: messageReceivedLocation.latitude,
                    long: messageReceivedLocation.longitude,
                    handler: { (status, geoLocation) in
                        if status {
                            cell.subtitleLabel.text! = "Message: \nGo to \(geoLocation) to open message"
                        }else {
                            cell.subtitleLabel.text! = "Message: \n\(messageReceived)"
                        }
                        
                })
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if self.segment.selectedSegmentIndex == 0 {
            if editingStyle == .delete {
                let removeGeo = self.sentMessages[indexPath.row]
                if let indexInArray = sentMessages.index(of: removeGeo) {
                    sentMessages.remove(at: indexInArray)
                        APIManager.defaultManager.removeLocationDart(withDart: removeGeo, handler: {
                            (msg, status) in
                            if !status {
                                self.showAlertWithTitle(title: "Failue", message: msg ?? "")
                                return
                            }
                            self.saveAllGeotificationMessages()
                        }
                    )
                }
            }
        }else {
            showAlertWithTitle(title: "Failure", message: "Can not delete received messages")
        }
    }

    
    
    // MARK: Private
    fileprivate func setupViews() {
        self.title = "Darts"
    }
    
    fileprivate func isMessageNearby(withCoordinate long: Double, lat:Double) -> Bool {
        let currentLat = CURRENT_USER?.lat ?? 0
        let currentLong = CURRENT_USER?.long ?? 0
        let coordinate₀ = CLLocation(latitude: lat, longitude: long)
        let coordinate₁ = CLLocation(latitude: currentLat, longitude: currentLong)
        let destance = ceil(coordinate₀.distance(from: coordinate₁) as Double)
        
        return destance <= 3000 ? true : false
        
    }
    
    fileprivate func getCurrentGeoLocation(lat: Double, long: Double, handler:@escaping (_ loaded: Bool, _ geoLocation: String)-> Void) {
        let location = CLLocation(latitude: lat,longitude: long)
        var locationStr = ""
        CLGeocoder().reverseGeocodeLocation(location, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    handler(false, locationStr)
                    return
                }
                if let placemarks = placemarks {
                    if placemarks.count > 0 {
                        let placemark = placemarks[0]
                        let administrativeArea = (placemark.locality != nil) ? placemark.locality! : " "
                        let city = (placemark.administrativeArea != nil) ? placemark.administrativeArea! : " "
                        let country = (placemark.country != nil) ? placemark.country! : " "
                        let loactionDescription = "\(country) \(city) \(administrativeArea)"
                        handler(true, loactionDescription)
                    }
                }
        })

    }
    
}
