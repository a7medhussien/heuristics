//
//  EditProfileViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/11/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import FirebaseDatabase

class EditProfileViewController: HeuristicsBaseViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
    }
    
    //MARK:- Private Helpers
    func configUI(){
        nameTextField.text = CURRENT_USER?.name ?? ""
        usernameTextField.text = CURRENT_USER?.userName ?? ""
    }
    
    //MARK:- @IBAction
    @IBAction func cancelTapped(_ sender: UIButton) {
        let user = CURRENT_USER
        if user?.userName == nil || user?.userName == "" {
            self.compeleteProfileWarining()
            return
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        guard nameTextField.validator.isNonEmpty() == nil, usernameTextField.validator.isNonEmpty() == nil else {
            self.showEmptyWarning()
            return
        }
        
        if let uid = CURRENT_USER?.userID {
            let name = nameTextField.text ?? ""
            let username = usernameTextField.text ?? ""
            
            CURRENT_USER?.name = name
            CURRENT_USER?.userName = username
            
            let firebaseDatabase = Database.database().reference()
            firebaseDatabase.child("users/\(uid)/Name").setValue(name)
            firebaseDatabase.child("users/\(uid)/username").setValue(username)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func alertSwitchChanged(_ sender: UISwitch) {
    }
    
    @IBAction func offersSwitchChanged(_ sender: UISwitch) {
    }
    
    @IBAction func visableSwitchChanged(_ sender: UISwitch) {
    }
    
    @IBAction func onlineSwitchChanged(_ sender: UISwitch) {
    }
    
    //MARK:- Private messages
    
    func showEmptyWarning(){
        self.showAlertWithTitle(title: "Complete empty fields",
                                message: "In order to complete your operation you must fill all fields")
    }
    
    func compeleteProfileWarining() {
        self.showAlertWithTitle(title: "Complete your profile",
                                message: "In order to use the app you should complete your profile by fill all empty files and click done to save the new data")
    }
}
