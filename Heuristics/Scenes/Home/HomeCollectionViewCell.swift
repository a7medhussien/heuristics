//
//  CategoryCollectionViewCell.swift
//  BTech
//
//  Created by Islam Abdelraouf on 7/4/17.
//  Copyright © 2017 RobustaStudio. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet var categoryImage: UIImageView!
    @IBOutlet var categoryLabel: UILabel!
    
}
