//
//  HomeViewViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/12/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn
import FBSDKLoginKit

class HomeViewViewController: HeuristicsBaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK:- location
    let locationManager = CLLocationManager()
    lazy var distanceFormatter: MKDistanceFormatter = MKDistanceFormatter()
    
    var cellReuseIdentifier = "HomeCollectionViewCell"
    var imgDataSource = [#imageLiteral(resourceName: "dartReceived"),#imageLiteral(resourceName: "alert"),#imageLiteral(resourceName: "friendship"),#imageLiteral(resourceName: "memory"),#imageLiteral(resourceName: "feedback"),#imageLiteral(resourceName: "invitation")]
    var titleDataSource = ["Darts", "Reminder", "Nearby\nFriends", "Memories", "Feedbacks", "Invite"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configTable()
        self.configUI()
        self.configLocation()
        self.configData()
    }
    
    //MARK:- Helpers
    
    func configData() {}
    
    func configTable()
    {
        self.collectionView.alwaysBounceVertical = false
        let cellNib = UINib(nibName: cellReuseIdentifier, bundle: Bundle.main)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: cellReuseIdentifier)
    }
    
    func configUI()
    {
        let logoutButton = UIBarButtonItem(image: #imageLiteral(resourceName: "logout"), style: .plain,
                                           target: self,
                                           action: #selector(logoutTapped(_:)))
        self.navigationItem.rightBarButtonItem = logoutButton
       
    }
    
    func configLocation() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    //MARK:- IBAction
    
    @IBAction func logoutTapped(_ sender: UIButton) {
        // Remove firebase data
        let firebaseAuth = Auth.auth()
        do { try firebaseAuth.signOut() }
        catch let signOutError as NSError { print ("Error signing out: %@", signOutError) }
        
        // logout from facebook and google
        GIDSignIn.sharedInstance().signOut()
        FBSDKLoginManager().logOut()
        
        if self.view.window?.rootViewController is LandingViewController {
            self.view.window?.rootViewController?.dismiss(animated: false, completion: {
                // Remove cached data and logout from system
            })
            return
        }
        
        let gateStoryboard = UIStoryboard(name: "Gate", bundle: nil)
        let landingViewController = gateStoryboard.instantiateViewController(withIdentifier: "landing")
        self.view.window?.rootViewController = landingViewController
        self.view.window?.rootViewController?.dismiss(animated: false, completion: {
            // Remove cached data and logout from system
        })
    }
    

}

extension HomeViewViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        
        case 0:
            let storyboard = UIStoryboard(name: "Darts", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GeoMessagesVC")
            self.pushVC(vc)
        case 1:
           let alertsVC = AlertsVC()
            self.pushVC(alertsVC)
        case 2:
            let nearbies = NearbyFriendsViewController()
            self.pushVC(nearbies)
        case 3:
            let memoryVC = MemoriesViewController()
            self.pushVC(memoryVC)
        case 4:// Feedbacks section
            let searchVC = SearchViewController()
            self.navigationController?.pushViewController(searchVC, animated: true)
        case 5:
            let inviteVC:InviteVC = InviteStoryboard.invite.viewController()
            self.pushVC(inviteVC)
        default:
            let offerView = OfferViewController()
            self.navigationController?.pushViewController(offerView, animated: true)
        }
    }
}

extension HomeViewViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = collectionView.frame.size.width/2 <= 320 ? collectionView.frame.size.width/2: collectionView.frame.size.width/4
        let height = collectionView.frame.size.height/3
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

extension HomeViewViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier,
                                                      for: indexPath) as! HomeCollectionViewCell
        
        cell.categoryImage.image = imgDataSource[indexPath.row]
        cell.categoryLabel.text = titleDataSource[indexPath.row]
        
        return cell
    }
    
}

extension HomeViewViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        if let uid = CURRENT_USER?.userID {
            //Set currnet date
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let result = formatter.string(from: date)
            
            let firebaseDatabase = Database.database().reference()
            firebaseDatabase.child("users/\(uid)/Long").setValue(locValue.longitude as Double)
            firebaseDatabase.child("users/\(uid)/Lat").setValue(locValue.latitude as Double)
            firebaseDatabase.child("users/\(uid)/ActiveTime").setValue(result)
        }
        CURRENT_USER?.long = locValue.longitude
        CURRENT_USER?.lat = locValue.latitude
        
    }
    
}
