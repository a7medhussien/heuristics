//
//  MemoriesCollectionViewCell.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 6/24/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class MemoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var memoryImgView: UIImageView!
}
