//
//  MemoriesViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class MemoriesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let cellReuseIdentifier = "MemoryTableViewCell"
    
    let titlesDataSource = ["WHAT A DAY!",
                            "LETS CELEBERATE!",
                            "DAY TO REMEMBER",
                            "FRIENDS AND NOTHING ELSE"]
    
    let dateDataSource = ["JAN 22, 2001",
                          "FEB 21, 2002",
                          "JULY 22, 2014",
                          "OCT 1, 2010"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
    }
    
    func configUI() {
        let memoryCellNib = UINib(nibName: cellReuseIdentifier, bundle: nil)
        tableView.register(memoryCellNib, forCellReuseIdentifier: cellReuseIdentifier)
        
        self.navigationItem.title = "Memories"
        
        let addGeoAlert = UIBarButtonItem(barButtonSystemItem: .add,
                                          target: self,
                                          action: #selector(addAlertPressed(_:)))
        self.navigationItem.rightBarButtonItem = addGeoAlert
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back",
                                                                style: .plain,
                                                                target: nil, action: nil)

    }
    
    func addAlertPressed(_ sender: UIButton) {
    }

}

extension MemoriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height/2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = MemoriesListViewController()
        self.pushVC(vc)
    }
}

extension MemoriesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier,
                                                 for: indexPath) as! MemoryTableViewCell
        cell.memoryImgView.image = UIImage(named: "mem\(indexPath.row % 4)")
        cell.memoryTitleLabel.text = titlesDataSource[indexPath.row]
        cell.memoryDateLabel.text = dateDataSource[indexPath.row]
        
        return cell
    }
}
