//
//  MemoryTableViewCell.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class MemoryTableViewCell: UITableViewCell {

    @IBOutlet weak var memoryBlurView: UIView!
    @IBOutlet weak var memoryTitleLabel: UILabel!
    @IBOutlet weak var memoryDateLabel: UILabel!
    @IBOutlet weak var memoryImgView: UIImageView!
    
}
