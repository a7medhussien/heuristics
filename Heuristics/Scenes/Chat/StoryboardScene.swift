//
//  StoryboardScene.swift
//  Heuristics
//
//  Created by Magdy Zamel on 4/16/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.

import UIKit

protocol StoryboardSceneType {
    static var storyboardName: String { get }
}


extension StoryboardSceneType {
    static func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.storyboardName, bundle: nil)
    }

    static func initialViewController() -> UIViewController {
        guard let vc = storyboard().instantiateInitialViewController() else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
     func viewController<T>() -> T {
        return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue) as! T
    }
}



enum  ChatSectionStoryboard : String {
    case chat = "ChatVC"
    case chatListVC = "ChatListVC"

}
enum  MainSectionsStoryboard: String {
    case home = "homeVC"
}


extension  ChatSectionStoryboard : StoryboardSceneType {
    static let storyboardName = "Chat"

    static func initialViewController() -> UINavigationController {
        guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
}
extension  MainSectionsStoryboard : StoryboardSceneType {
    static let storyboardName = "Main"

    static func initialViewController() -> BaseTabBarViewController {
        guard let vc = storyboard().instantiateInitialViewController() as? BaseTabBarViewController else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
}

enum  InviteStoryboard: String {
    case invite = "inviteVC"
}

enum  AlertsStoryboard: String {
    case home = "Alerts"
}

extension  InviteStoryboard : StoryboardSceneType {
    static let storyboardName = "Invite"

    static func initialViewController() -> BaseNavigationViewController {
        guard let vc = storyboard().instantiateInitialViewController() as? BaseNavigationViewController else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
}
extension  AlertsStoryboard : StoryboardSceneType {
    static let storyboardName = "Alerts"
    
    static func initialViewController() -> BaseNavigationViewController {
        guard let vc = storyboard().instantiateInitialViewController() as? BaseNavigationViewController else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
}

