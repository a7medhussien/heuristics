//
//  LastMessageCell.swift
//  Heuristics
//
//  Created by Magdy Zamel on 4/16/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import Foundation
import Kingfisher
import FirebaseAuth

class LastMessageCell: UITableViewCell {
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var userImageView:UIImageView!
    @IBOutlet weak var msgLabel: UILabel!

    @IBOutlet weak var TimeStampLabel: UILabel!
    var imageUrl:String?

    var dataSource:LastMessage!

    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.userImageView.layer.cornerRadius = self.userImageView.frame.height / 2
        }
    }

    func setupWithDataSource(lastMessage:LastMessage){
        self.dataSource  = lastMessage
        msgLabel.text = dataSource.msg
        TimeStampLabel.text = dataSource.timeStamp
        senderNameLabel.text = dataSource.receiverName
        if dataSource.receiverName == CURRENT_USER?.name{
            senderNameLabel.text = dataSource.senderName
        }

//        let url = URL(string: imageUrl ?? "")
//        userImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "avatar3"), options: [.transition(ImageTransition.fade(1))], progressBlock: { receivedSize, totalSize in
//        }, completionHandler: { image, error, cacheType, imageURL in
//        })

    }

}
