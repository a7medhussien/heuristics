//
//  ChatListVC.swift
//  Heuristics
//
//  Created by Magdy Zamel on 4/16/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import Foundation
import FirebaseDatabase
import ObjectMapper
import FirebaseAuth

class ChatListVC: UIViewController ,UsersSelectionDelegate{

    @IBOutlet weak var chatListTableView: UITableView!

    private lazy var chatListRef: DatabaseReference = Database.database().reference().child("Chats").child("\(Auth.auth().currentUser!.uid)").child("ChatList")
    var chatListMessages = [LastMessage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back",
                                                                style: .plain,
                                                                target: nil, action: nil)
        chatListRef.queryOrdered(byChild: "timeStamp").observe(.childChanged, with: chatListSnapShotCallBack)
        chatListRef.queryOrdered(byChild: "timeStamp").observe(.childAdded, with: chatListSnapShotCallBack)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        chatListTableView.reloadData()
    }

    func chatListSnapShotCallBack(snapshot:DataSnapshot) {
        if !snapshot.exists() {
            showErrorAlert(withMessage:"Sorry, sever error try agin later")
        }

        let messageData = snapshot.value as! Dictionary<String,Any>
        let message:LastMessage! =  Mapper<LastMessage>().map(JSON:messageData)
        if let index = chatListMessages.index(where: {$0.ownerUserID == message!.ownerUserID }){
            chatListMessages[index] = chatListMessages[0]
            chatListMessages[0] = message
        }else{
            chatListMessages.insertFirst(message)
        }
        chatListTableView.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nav = segue.destination as? BaseNavigationViewController {
            if let vc = nav.viewControllers.first as? UsersListVC {
                vc.delegate = self
            }
        }
    }
    
    func didSelectToBeginChatWith(User user: User) {
        let vc:ChatVC = ChatSectionStoryboard.chat.viewController()
        vc.title = user.name!
        vc.receiverName = user.name!
        vc.ownerId = user.userID!
        vc.receiverId = user.userID!
        
        self.pushVC(vc)
    }

}

extension ChatListVC : UITableViewDelegate, UITableViewDataSource {
    //MARK: - tableView dataSource and delegate-
    func numberOfSections(in tableView: UITableView) -> Int {
        return chatListMessages.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LastMessageCell") as! LastMessageCell
        cell.setupWithDataSource(lastMessage: chatListMessages[indexPath.section])
        return cell
    }
    //notes 
    // add reviews numbers

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatVC:ChatVC = ChatSectionStoryboard.chat.viewController()
        if chatListMessages[indexPath.section].receiverName == CURRENT_USER?.name{
            chatVC.receiverName = chatListMessages[indexPath.section].senderName
            chatVC.title = chatListMessages[indexPath.section].senderName

        }else{
            chatVC.receiverName = chatListMessages[indexPath.section].receiverName
            chatVC.title = chatListMessages[indexPath.section].receiverName
        }
        chatVC.ownerId = chatListMessages[indexPath.section].ownerUserID
        if chatListMessages[indexPath.section].receiverId == CURRENT_USER?.userID! {
            chatVC.receiverId = chatListMessages[indexPath.section].senderId
        }else{
            chatVC.receiverId = chatListMessages[indexPath.section].receiverId
        }
        self.pushVC(chatVC)
    }

}

