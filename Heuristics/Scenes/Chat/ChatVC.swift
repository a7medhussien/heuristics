//
//  ChatVC.swift
//  Heuristics
//
//  Created by Magdy Zamel on 4/16/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Kingfisher
import FirebaseDatabase
import OneSignal
import EZSwiftExtensions
import FirebaseAuth

enum parentType {
    case nearbyPeople
    case userList
}

class ChatVC: JSQMessagesViewController ,UIGestureRecognizerDelegate,UITextFieldDelegate{


    private var messages = [JSQMessage] ()

    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()

    lazy var  messageRef = Database.database().reference().child("ChatMSGs")
    private var newMessageRefHandle: DatabaseHandle?

    private lazy var senderLastMessageRef: DatabaseReference! = Database.database().reference().child("Chats")
    private lazy var receiverLastMessageRef: DatabaseReference! = Database.database().reference().child("Chats")

    var receiverName:String = ""
    var receiverId:String = ""
    var ownerId:String = ""
    var senderName:String = ""
    var viewType: parentType = .userList
    
    func configUI()  {
        
        if viewType == .nearbyPeople {
            let closeButton = UIBarButtonItem(title: "Close", style: .plain, target: self,
                                              action: #selector(self.dismissView(_:)))
            self.navigationItem.leftBarButtonItem = closeButton
        }
        
        if let uid = Auth.auth().currentUser?.uid, CURRENT_USER == nil  {
            APIManager.defaultManager.getUser(uid: uid, completionHandler: {[unowned self] (user, success, message) in
                if success && user != nil {
                    CURRENT_USER =  user
                    self.configUI()
                }
            })
        }
        else {
            self.senderId = Auth.auth().currentUser!.uid
            self.senderName = CURRENT_USER!.name!
            self.senderDisplayName = CURRENT_USER!.name!
            

            if ownerId ==  self.senderId! {
                messageRef = messageRef.child("\(receiverId)Heuristic\(ownerId)")
                print("\(receiverId)Heuristic\(ownerId)")
                senderLastMessageRef = senderLastMessageRef.child("\(senderId!)").child("ChatList").child("\(receiverId)")
                receiverLastMessageRef = receiverLastMessageRef.child("\(receiverId)").child("ChatList").child("\(receiverId)")
            }else {
                messageRef = messageRef.child("\(senderId!)Heuristic\(ownerId)")
                senderLastMessageRef = senderLastMessageRef.child("\(senderId!)").child("ChatList").child("\(receiverId)")
                receiverLastMessageRef = receiverLastMessageRef.child("\(receiverId)").child("ChatList").child("\(senderId!)")
            }


            setupNavBar()

            collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
            collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero

            //to hide attach button
            self.inputToolbar.contentView.leftBarButtonItem.setImage(nil, for: .normal)
            self.inputToolbar.contentView.leftBarButtonItem.setTitle(nil, for: .normal)

            self.inputToolbar.contentView.leftBarButtonItem.setImage(nil, for: .focused)
            self.inputToolbar.contentView.leftBarButtonItem.setTitle(nil, for: .focused)

            self.inputToolbar.contentView.leftBarButtonItem.setImage(nil, for: .highlighted)
            self.inputToolbar.contentView.leftBarButtonItem.setTitle(nil, for: .highlighted)

            self.inputToolbar.contentView.leftBarButtonItem.setImage(nil, for: .selected)
            self.inputToolbar.contentView.leftBarButtonItem.setTitle(nil, for: .selected)

            observeMessages()
        }




    }
    override func viewDidLoad() {
        super.viewDidLoad()
         senderId = ""
        senderDisplayName = ""
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.none;
        configUI()
    }


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
    }
    override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "dd MMM yyyy"

        return NSAttributedString(string:  dateFormatter.string(from: messages[indexPath.item].date))
    }





    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        if message.senderId == senderId! {
            cell.cellBottomLabel.textColor =  UIColor.yellow
            cell.textView?.textColor = UIColor.white
        } else {
            cell.cellBottomLabel.textColor =  UIColor.yellow

            cell.textView?.textColor = UIColor.black
        }
        return cell
    }



    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId! {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }

    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let itemRef = messageRef.childByAutoId() // 1
        let messageItem :[String : Any] = [
            "receiverName": receiverName,
            "ownerUserID" : ownerId,
            "msg" :  text!,
            "receiverId" : receiverId,
            "senderId" : senderId!,
            "senderName" : senderName,
            "timeStamp" : date.timeIntervalSince1970*1000
        ]
        let messageBody = MessageBodyNotification(userId: receiverId, contentMessage: text!, senderName: senderName)
        APIManager.defaultManager.postMessageWithBody(messageBody: messageBody)

        itemRef.setValue(messageItem) // 3
        senderLastMessageRef.setValue(messageItem)
        receiverLastMessageRef.setValue(messageItem)
        JSQSystemSoundPlayer.jsq_playMessageSentSound() // 4
        finishSendingMessage() // 5
    }


    private func observeMessages() {
        newMessageRefHandle = messageRef.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData = snapshot.value as! Dictionary<String,Any>

            if let id = messageData["senderId"] as? String,
                let name = messageData["senderName"] as? String,
                let text = messageData["msg"] as? String,
                let timeStamp = messageData["timeStamp"] as?  Double ,
                !text.isEmpty {
                let date = NSDate(timeIntervalSince1970:TimeInterval(timeStamp/1000))
                self.addMessage(withSenderId: id, name: name, text: text, date: date as Date)
                JSQSystemSoundPlayer.jsq_playMessageSentSound()
                self.finishReceivingMessage()
            } else {
                print("Error! Could not decode message data")
            }
        })
    }

    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: appColor)
    }

    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }

    private func addMessage(withSenderId id: String, name: String, text: String,date:Date) {
        if let message = JSQMessage(senderId: id, senderDisplayName: name,  date:date, text: text ) {
            messages.append(message)
        }
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    func cancelButtonTapped() {
        self.popVC()
    }

    private func setupNavBar() {

        let rightButton = UIButton(type: .system)
        let sendImage = #imageLiteral(resourceName: "ic_sendmdpi")
        rightButton.tintColor = appColor
        rightButton.setImage(sendImage, for: UIControlState.normal)
        
        self.inputToolbar.contentView.rightBarButtonItemWidth = CGFloat(34.0)
        
        self.inputToolbar.contentView.rightBarButtonItem = rightButton
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "userAvatar"))
        imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        DispatchQueue.main.async {
            imageView.clipsToBounds = true
            imageView.layer.cornerRadius = imageView.frame.height / 2
            imageView.contentMode = .scaleAspectFill
//            let url = URL(string: URL_IMAGE_PREFIX + "profiles/" + ((self.channel?.image) ?? ""))
//            imageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatarmdpi"), options: [.transition(ImageTransition.fade(1))], progressBlock: { receivedSize, totalSize in
//            }, completionHandler: { image, error, cacheType, imageURL in
                imageView.layer.cornerRadius = imageView.frame.height / 2
                imageView.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
                imageView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
//            })
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: imageView)
        }
    }

    @IBAction func dismissView(_ sender: UIBarButtonItem) {
        self.dismissVC(completion: nil)
    }
}

