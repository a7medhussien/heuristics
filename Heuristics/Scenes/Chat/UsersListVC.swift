 //
//  MessagesViewController.swift
//  Locationapp
//
//  Created by Mostafa El_sayed on 11/25/17.
//  Copyright © 2017 HeuristicGang. All rights reserved.
//

import UIKit
import XLPagerTabStrip
 
 protocol UsersSelectionDelegate:class {
    func didSelectToBeginChatWith(User user:User)
 }
 
class UsersListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    lazy var users: [User] = [User]()
    lazy var usersFiltered: [User] = [User]()
    weak var delegate:UsersSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUsers()
    }
    

    // MARK: - @IBActions

    
    // MARK - Helper methods
    fileprivate func loadUsers() {
        APIManager.defaultManager.getUsers { (users, status, msg) in
            if status {
                
                self.users = users!
                self.usersFiltered = self.users
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
}
extension UsersListVC: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersFiltered.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! LastMessageCell
        cell.senderNameLabel.text! = usersFiltered[indexPath.row].name ?? ""
        cell.msgLabel.text! = usersFiltered[indexPath.row].email  ?? ""
        return cell
    }
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismissVC {
            self.delegate?.didSelectToBeginChatWith(User: self.users[indexPath.row])
        }
    }
    
    @IBAction func cancelTapped(){
        dismissVC(completion: nil)
    }
}
 
 // MARK: - IndicatorInfoProvider
 extension UsersListVC: IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "People"
    }
 }




