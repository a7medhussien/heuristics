//
//  OfferDetailViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/17/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import MapKit

class OfferDetailViewController: UIViewController {

    @IBOutlet weak var shadowBackgroundView: UIView!
    @IBOutlet weak var offerContainerView: UIView!
    @IBOutlet weak var offerImgView: UIImageView!
    @IBOutlet weak var offerTitleLabel: UILabel!
    @IBOutlet weak var offerDescriptionLabel: UILabel!
    
    var img: UIImage?
    var titles: String?
    var descriptions: String?
    var link: String?
    var long: Double?
    var lat: Double?

    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.confingPresnation()
    }
    
    func configUI() {
        self.offerTitleLabel.text = titles
        self.offerDescriptionLabel.text = descriptions
        self.offerImgView.image = img
        
        self.configUIGestures()
        self.offerContainerView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
        self.shadowBackgroundView.alpha = 0.0
    }
    
    func confingPresnation() {
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.offerContainerView.transform = CGAffineTransform.identity
            self?.shadowBackgroundView.alpha = 0.8
        }
    }
    
    func configUIGestures() {
        // Dismiss view by dragging it out
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))
        self.offerContainerView.addGestureRecognizer(panGesture)
        
        // Dismiss the screen when user tap on shadow background
        let tapOnShadow = UITapGestureRecognizer(target: self, action: #selector(openPromoLink))
        self.shadowBackgroundView.addGestureRecognizer(tapOnShadow)

        let tapOnOffer = UITapGestureRecognizer(target: self, action: #selector(openPromoLink))
        self.offerContainerView.addGestureRecognizer(tapOnOffer)
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(_:)))
        self.offerContainerView.addGestureRecognizer(longPressGesture)
    }
    
    func panGestureAction(_ gesture:UIPanGestureRecognizer){
        let shadowHieght = self.view.frame.height
        let shadowWidth = self.view.frame.width
        
        if gesture.state == .began {
            let offerContainerViewWidth = self.offerContainerView.frame.width
            let offerContainerViewHeight = self.offerContainerView.frame.height
            let startPoint = CGPoint(x: shadowWidth/2 - offerContainerViewWidth/2,
                                     y: shadowHieght/2 - offerContainerViewHeight/2)
            gesture.setTranslation(startPoint, in: self.offerContainerView)
            return
        }
        
        let translation = gesture.translation(in: self.offerContainerView)
        self.offerContainerView.frame.origin = translation
        
        if gesture.state == .ended {
            let viewDraggingVolicty = gesture.velocity(in: view)
            if viewDraggingVolicty.y >= 1500 {
                self.offerContainerView.removeFromSuperview()
                self.dismissView()
                return
            }
            UIView.animate(withDuration: 0.3, animations: {
                self.offerContainerView.center = CGPoint(x: shadowWidth/2, y: shadowHieght/2)
            })
        }
    }
    
    func handleLongPress(_ longPressGesture:UILongPressGestureRecognizer) {
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(self.lat ?? 0.0, self.long ?? 0.0)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.titles ?? "Unknown"
        mapItem.openInMaps(launchOptions: options)
    }
    
    func openPromoLink(){
        if let url = URL(string: link ?? "") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    func dismissView() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.shadowBackgroundView.alpha = 0.0
            self?.offerContainerView.transform = CGAffineTransform(translationX: 0, y: self?.view.frame.height ?? 500)
        }) {[weak self] (_) in
            self?.dismiss(animated: false, completion: nil)
        }
    }
    
}
