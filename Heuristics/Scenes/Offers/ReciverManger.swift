//
//  ReciverManger.swift
//  Heuristics
//
//  Created by Magdy Zamel on 6/22/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import Foundation
import CoreBluetooth
class ReciverManger :NSObject{
    static var shared : ReciverManger!
    var peripheralManager:CBPeripheralManager!
    let serviceUUID = CBUUID(string: "0C50D390-DC8E-436B-8A00-A36D1B304B18")
    let dataCollectionCharacteristicUUID = CBUUID(string: "0C50D390-DC8E-436B-8AD0-A36D1B304B18")
    let advertisingIdentifier = "myperipheralNameTag"

    var bluetoothState:Dynamic<Int>!
    var reloadState:Dynamic<Bool>!

    override init() {
        super.init()
        self.peripheralManager = CBPeripheralManager(delegate: self , queue: nil)
        bluetoothState =  Dynamic(0)
        reloadState =  Dynamic(false)

        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true, block: { (_) in
            let lastArray  = adsArray
            adsArray = [AdsModel]()
            ads.forEach({ (key,adds) in
                adsArray.append(adds)
                if (Date().timeIntervalSince1970 - adds.timeStamp!) > 15 {
                    ads.removeValue(forKey: key)
                }
            })
            var reload = (lastArray.count != adsArray.count)
            if !reload{
                lastArray.forEach({ (ads) in
                    if (adsArray.filter{ $0.uuid == ads.uuid }.isEmpty){
                        reload = true
                        return
                    }
                })
            }
            if reload{

                self.reloadState.value = true
            }
        })
    }
}

extension ReciverManger:CBPeripheralManagerDelegate{

    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {

        //        peripheralManager.stopAdvertising()

        if  peripheral.state == .poweredOn {
            var advertisementData = [String: Any]()
            advertisementData[CBAdvertisementDataServiceUUIDsKey] = [serviceUUID]
            advertisementData[CBAdvertisementDataLocalNameKey] = self.advertisingIdentifier

            let characteristic = CBMutableCharacteristic(
                type: dataCollectionCharacteristicUUID,
                properties: .write,
                value: nil,
                permissions: CBAttributePermissions.writeable)
            let dataService = CBMutableService(type: serviceUUID, primary: true)
            dataService.characteristics = [characteristic]
            peripheralManager.add(dataService)

            peripheralManager.startAdvertising(advertisementData)
        }
        bluetoothState.value =  peripheral.state.rawValue

    }

}


extension ReciverManger {
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        for request in requests{
            if request.characteristic.uuid == dataCollectionCharacteristicUUID {
                if let data = request.value{
                    let adds = try? JSONDecoder().decode(AdsModel.self, from: data)
                    ads [adds!.uuid!] = adds
                    peripheral.respond(to: request, withResult: .success)
                }
            }
        }
    }
}
