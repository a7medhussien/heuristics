//
//  OfferTableViewCell.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/14/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {

    @IBOutlet weak var adTitleLabel: UILabel!
    @IBOutlet weak var adDescLabel: UILabel!
    @IBOutlet weak var adImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
