//
//  OfferViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/14/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import CoreBluetooth
import Kingfisher

var ads  = [String:AdsModel]()
var adsArray  = [AdsModel]()

class OfferViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var discriptionLable: UILabel!

    let offerCellReuseIdentifier = "OfferTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        ReciverManger.shared.bluetoothState.bindAndFire { [unowned self] in
            let centralstate = CBManagerState(rawValue: $0)!
            var message = ""
            switch centralstate {
            case .unknown:
                message = "The state of the BLE Manager is unknown."
            case .poweredOff:
                message = "Bluetooth on this device is currently powered off."
            case .unsupported:
                message = "This device does not support Bluetooth Low Energy."
            case .unauthorized:
                message = "This app is not authorized to use Bluetooth Low Energy."
            case .resetting:
                message = "The BLE Manager is resetting; a state update is pending."
            case .poweredOn:
                message = "Bluetooth LE is turned on and ready for communication."
            }
            self.discriptionLable.text = message
        }
        ReciverManger.shared.reloadState.bindAndFire { (relaod) in
            if relaod {
                self.tableView.reloadData()
            }
        }

    }



    func configUI() {
        let offerCellNib = UINib(nibName: offerCellReuseIdentifier, bundle: nil)
        tableView.register(offerCellNib, forCellReuseIdentifier: offerCellReuseIdentifier)

        self.navigationItem.title = "Offers"
    }


    func getImageIndex(named str: String)->Int {
        let index1 = str[0].ascii ?? 0
        let index2 = str.last?.ascii ?? 0
        return Int((index1 + index2) % 10)
    }
}

extension OfferViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let offerDetailViewController = OfferDetailViewController()
        offerDetailViewController.modalPresentationStyle = .overFullScreen
        let ad = adsArray[0].ads[indexPath.row]
        let index = getImageIndex(named: ad.title ?? "nothing")
        offerDetailViewController.img = UIImage(named: "offer\(index)")
        offerDetailViewController.titles = ad.title
        offerDetailViewController.descriptions = ad.details
        offerDetailViewController.long = ad.lng
        offerDetailViewController.lat = ad.lat
        offerDetailViewController.link = ad.websiteURl
    self.present(offerDetailViewController, animated: false, completion: nil)
    }
}

extension OfferViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if adsArray.count == 0 {
            return 0
        }
        return adsArray[0].ads.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: offerCellReuseIdentifier,
                                                 for: indexPath) as! OfferTableViewCell
        let ad = adsArray[0].ads[indexPath.row]
        cell.adTitleLabel.text = ad.title
        cell.adDescLabel.text = ad.details
        let index = getImageIndex(named: ad.title ?? "nothing")
        cell.adImgView.image = UIImage(named: "offer\(index)")
        return cell
    }
}
