//
//  UserTableViewCell.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 6/21/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
