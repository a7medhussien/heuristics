//
//  UsersListViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 6/21/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

protocol UserListDataSoruce {
    func didselect(User user: User?)
}

enum ListViewType {
    case following
    case all
}

class UsersListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var users = [User?]()
    var dataSource: UserListDataSoruce?
    var listType: ListViewType = .all
    fileprivate let cellReuseIdentifier = "UserTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        configData()
    }

    func configUI() {
        self.title = "Find Your Friend"
        if listType == .all {
            let closeButton = UIBarButtonItem(title: "Close", style: .plain, target: self,
                                              action: #selector(self.closeTapped(_:)))
            self.navigationItem.leftBarButtonItem = closeButton
        }
        
        
        let followCell = UINib(nibName: cellReuseIdentifier, bundle: Bundle.main)
        self.tableView.register(followCell, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func configData() {
        if listType == .all  { getAllUsers() }
        else { getFollowingUsers() }
    }
    
    func getFollowingUsers() {
        APIManager.defaultManager.getFollowing {[weak self] (following, success, message) in
            if success { self?.users = following; self?.tableView.reloadData() }
        }
    }
    
    func getAllUsers() {
        APIManager.defaultManager.getUsers {[weak self] (users, success, message) in
            if success { self?.users = users ?? []; self?.tableView.reloadData() }
        }
    }
    
    
    @IBAction func closeTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension UsersListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dataSource?.didselect(User: users[indexPath.row])
        if listType == .all {
            let friendDetailViewController = FriendDetailsViewController()
            friendDetailViewController.modalPresentationStyle = .overFullScreen
            friendDetailViewController.user = users[indexPath.row]
            friendDetailViewController.type = .unknown
            self.present(friendDetailViewController, animated: false, completion: nil)
        }else {
            self.popVC()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}

extension UsersListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier,
                                                 for: indexPath) as! UserTableViewCell
        cell.nameLabel.text = users[indexPath.row]?.name ?? ""
        cell.usernameLabel.text = users[indexPath.row]?.userName ?? ""
        let placeHolder = users[indexPath.row]?.gender == 0 ? #imageLiteral(resourceName: "avatar0") : #imageLiteral(resourceName: "avatar1")
        cell.userImgView.loadImage(users[indexPath.row]?.imageUrl ?? "", placeHolder)
        return cell
    }
}
