//
//  FriendCollectionViewCell.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class FriendCollectionViewCell: UICollectionViewCell {

    @IBOutlet var friendImage: UIImageView!
    @IBOutlet var friendTitleLabel: UILabel!
    @IBOutlet var friendDecLabel: UILabel!

}
