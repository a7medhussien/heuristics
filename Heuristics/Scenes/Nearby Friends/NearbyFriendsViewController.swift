//
//  NearbyFriendsViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation
import Kingfisher

class NearbyFriendsViewController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loadingView: UIView!
    
    var cellReuseIdentifier = "FriendCollectionViewCell"
    var users: [User] = []
    
    var refreshButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        self.configCollection()
        self.configData()
    }
    
    //MARK:- Configures
    
    func configUI() {
        self.navigationItem.title = "Nearby friends"
        
        refreshButton = UIBarButtonItem(image: #imageLiteral(resourceName: "refresh-arrow"), style: .plain, target: self,
                                        action: #selector(self.refresh(_:)))
        self.navigationItem.rightBarButtonItem = refreshButton
        
    }
    
    func configCollection()
    {
        self.collectionView.alwaysBounceVertical = false
        let cellNib = UINib(nibName: cellReuseIdentifier, bundle: Bundle.main)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: cellReuseIdentifier)
    }
    
    func configData() {
        APIManager.defaultManager.getUsers {[weak self] (users, success, message) in
            
            self?.loadingView.isHidden = true
            
            if success {
                self?.navigationItem.rightBarButtonItem = self?.refreshButton
                
                let allUsers = users ?? []
                var nearbyUsers: [User] = []
                for user in allUsers {
                    //Calculate distance and check location exist or not
                    if  let userlat = user.lat,
                        let userLong = user.long,
                        let currentLat = CURRENT_USER?.lat,
                        let currentLong = CURRENT_USER?.long,
                        user.userID != CURRENT_USER?.userID ?? "0"
                    {
                        let coordinate₀ = CLLocation(latitude: currentLat, longitude: currentLong)
                        let coordinate₁ = CLLocation(latitude: userlat, longitude: userLong)
                        let destance = ceil(coordinate₀.distance(from: coordinate₁) as Double)
                        user.distanceFromCurrentUser = destance
                        nearbyUsers.append(user)
                    }
                }
                
                nearbyUsers = nearbyUsers.sorted(by: {
                    $0.distanceFromCurrentUser ?? 0 < $1.distanceFromCurrentUser ?? 0
                })
                self?.users = nearbyUsers
                self?.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func refresh(_ sender: UIBarButtonItem) {
        
        let uiBusy = UIActivityIndicatorView(activityIndicatorStyle: .white)
        uiBusy.hidesWhenStopped = true
        uiBusy.startAnimating()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: uiBusy)
        
        configData()
    }
}

extension NearbyFriendsViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let friendDetailViewController = FriendDetailsViewController()
        friendDetailViewController.modalPresentationStyle = .overFullScreen
        friendDetailViewController.user = users[indexPath.row]
        self.present(friendDetailViewController, animated: false, completion: nil)
    }
}

extension NearbyFriendsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = collectionView.frame.size.width/2 <= 320 ? collectionView.frame.size.width/2: collectionView.frame.size.width/4
        let height = collectionView.frame.size.height/3 + 20
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

extension NearbyFriendsViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier,
                                                      for: indexPath) as! FriendCollectionViewCell
        
        
        let destance = users[indexPath.row].distanceFromCurrentUser ?? 0
        var formatedDestance = destance >= 1000 ? "\(ceil(destance/1000).cleanValue) km" : "\(ceil(destance).cleanValue) meter"
        formatedDestance = destance <= 20 ? "very close" : formatedDestance
        
        users[indexPath.row].img = UIImage(named: "avatar\(indexPath.row%3+1)")
        
        cell.friendTitleLabel.text = (users[indexPath.row].name ?? "") + "\n" + formatedDestance
        cell.friendImage.loadImage(users[indexPath.row].imageUrl ?? "", users[indexPath.row].gender == 0 ? #imageLiteral(resourceName: "avatar0") : #imageLiteral(resourceName: "avatar1"))
        
        
        let activeFrom = Date().timeIntervalSince(users[indexPath.row].getDate())/60
        var decText = activeFrom <= 2 ? "Active Now" : "Active from \(ceil(activeFrom).cleanValue) min"
        decText = activeFrom >= 60 ? "Active from \(ceil(activeFrom/60).cleanValue) hr" : decText
        decText = activeFrom >= 1440 ? "Active from \(ceil(activeFrom/1440).cleanValue) Day" : decText
        decText = activeFrom >= 43800 ? "Active from \(ceil(activeFrom/43800).cleanValue) Month" : decText
        decText = activeFrom >= 525600 ? "Active from \(ceil(activeFrom/525600).cleanValue) Year" : decText
        cell.friendDecLabel.text = decText
        
        return cell
    }
    
}
