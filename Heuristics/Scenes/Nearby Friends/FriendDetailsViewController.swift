//
//  FriendDetailsViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 4/29/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import MapKit

enum DetailsType {
    case friend
    case unknown
}

class FriendDetailsViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var profileContainerView: UIView!
    @IBOutlet weak var actionsContainerView: UIView!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var fullNameLable: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var numberOfFollowingLabel: UILabel!
    @IBOutlet weak var numberOfFollowersLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    var user: User?
    var type: DetailsType = .friend
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        configData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configUIAppearance()
    }
    
    func configUI() {
        // Dismiss the screen when user tap on shadow background
        let tapOnShadow = UITapGestureRecognizer(target: self, action: #selector(dismissView(_:)))
        self.backgroundView.addGestureRecognizer(tapOnShadow)
        
        // Animate views
        self.backgroundView.alpha = 0.0
        self.profileContainerView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
        self.actionsContainerView.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height)
        
        if self.type == .unknown {
            self.actionsContainerView.isHidden = true
        }
    }
    
    func configUIAppearance() {
        UIView.animate(withDuration: 0.3) { [unowned self] in
            self.backgroundView.alpha = 1
            self.profileContainerView.transform = CGAffineTransform.identity
            self.actionsContainerView.transform = CGAffineTransform.identity
        }
    }
    
    func configData() {
        self.fullNameLable.text = user?.name ?? ""
        self.usernameLabel.text = user?.userName ?? ""
        self.numberOfFollowingLabel.text = user?.following.count.toString ?? "0"
        self.numberOfFollowersLabel.text = user?.followers.count.toString ?? "0"
        self.profileImgView.image = user?.gender == 0 ? #imageLiteral(resourceName: "avatar0") : #imageLiteral(resourceName: "avatar1")
        
        if checkFollowState(withUid: user?.userID ?? "") {
            followButton.isSelected = true
            followButton.setTitle("Following", for: .selected)
            followButton.setBackgroundColor(UIColor.darkGreen, forState: .normal)
        }
    }
    
    @IBAction func followingButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func followersButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func followButtonTapped(_ sender: UIButton) {
        
        if checkFollowState(withUid: user?.userID ?? "") {
            APIManager.defaultManager.unfollow(withUid: user?.userID)
            sender.unfollow()
            return
        }
        sender.startAnimating()
        APIManager.defaultManager.follow(withUid: user?.userID) {[weak self] (message, success) in
            if success { sender.follow() }
            else { self?.showErrorAlert(withMessage: message ?? "") }
            sender.stopAnimating()
        }
        
    }
    
    @IBAction func navigateButtonTapped(_ sender: UIButton) {
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(user?.lat ?? 0.0, user?.long ?? 0.0)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = user?.name ?? "Unknown"
        mapItem.openInMaps(launchOptions: options)
    }
    
    @IBAction func messageButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func chatButtonTapped(_ sender: UIButton) {
        let vc:ChatVC = ChatSectionStoryboard.chat.viewController()
        vc.viewType = .nearbyPeople
        vc.title = user?.name ?? ""
        vc.receiverName = user?.name ?? ""
        vc.ownerId = user?.userID ?? ""
        vc.receiverId = user?.userID ?? ""
        let nav = BaseNavigationViewController(rootViewController: vc)
        self.presentVC(nav)
    }
    
    @IBAction func phoneButtonTapped(_ sender: UIButton) {
        guard let number = URL(string: "tel://" + (user?.phoneNumber ?? "0")) else {
            self.showErrorAlert(withMessage: "This phone number is private number")
            return
        }
        UIApplication.shared.open(number)
    }
    
    @IBAction func dismissView(_ tapGesture:UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            
            self.backgroundView.alpha = 0.0
            self.profileContainerView.transform = CGAffineTransform(translationX: 0,
                                                                    y: self.view.frame.height)
            self.actionsContainerView.transform = CGAffineTransform(translationX: 0,
                                                                    y: self.view.frame.height)
            
        }) {[unowned self] (_)  in
            self.dismiss(animated: false, completion: nil)
        }
    }
}
