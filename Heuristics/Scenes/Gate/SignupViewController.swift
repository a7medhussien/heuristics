//
//  SignupViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/8/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func signinButtonTapped(_ sender: UIButton) {
        guard validation() == true else { return }

        sender.startWhiteAnimating()

        APIManager.defaultManager.signup(withEmail: emailTextField.text ?? "",
                                         password: passwordTextField.text ?? "")
        {[weak self] (message, uid, success) in

            sender.stopAnimating()

            if success {
                let storyboard = UIStoryboard(name: "Gate", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "siginupInfo")
                (vc as? CompleteProfileViewController)?.uuid = uid ?? ""
                self?.present(vc, animated: true, completion: nil)
                return
            }

            self?.showErrorAlert(withMessage: message ?? "")

        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Private Helpers
    
    fileprivate func validation() -> Bool {
        let title = "Validaton Error"
        
        if let mssg = emailTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = emailTextField.validator.isValidEmail() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = passwordTextField.validator.verifyPassword(With: confirmPasswordTextField) {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = passwordTextField.validator.isValidePassword() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        return true
    }
}
