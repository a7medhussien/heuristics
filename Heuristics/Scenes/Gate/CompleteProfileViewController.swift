//
//  completeProfileViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 6/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn
import FBSDKLoginKit

class CompleteProfileViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var maleFlagImgView: UIImageView!
    @IBOutlet weak var femaleFlagImgView: UIImageView!
    @IBOutlet weak var logoutButton: UIButton!
    
    var uuid: String?
    var gender = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let mail = CURRENT_USER?.email, mail != "" {
            emailTextField.text = mail
            emailTextField.isUserInteractionEnabled = false
            emailTextField.alpha = 0.8
        }
        nameTextField.text = CURRENT_USER?.name ?? ""
        usernameTextField.text = CURRENT_USER?.userName ?? ""
    }

    //MARK:- Helpers
    
    fileprivate func validation() -> Bool {
        let title = "Validaton Error"
        
        if let mssg = emailTextField.validator.isValidEmail() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = nameTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = usernameTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        if let mssg = phoneNumberTextField.validator.isNonEmpty() {
            self.showErrorAlert(withTitle: title, message: mssg); return false
        }
        
        return true
    }
    
    
    @IBAction func selectButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func clearButtonTapped(_ sender: UIButton) {
    }

    @IBAction func maleGestureTapped(_ sender: UITapGestureRecognizer) {
        self.gender = 1
        self.imgView.image = #imageLiteral(resourceName: "avatar1")
        self.maleFlagImgView.image = #imageLiteral(resourceName: "tick")
        self.femaleFlagImgView.image = UIImage()
    }
    
    @IBAction func femaleGestureTapped(_ sender: UITapGestureRecognizer) {
        self.gender = 0
        self.imgView.image = #imageLiteral(resourceName: "avatar0")
        self.maleFlagImgView.image = UIImage()
        self.femaleFlagImgView.image = #imageLiteral(resourceName: "tick")
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        guard validation() == true else { return }
        
        APIManager.defaultManager.setUserData(withUid: uuid ?? "",
                                              email: emailTextField.text ?? "",
                                              name: self.nameTextField.text ?? "",
                                              username: self.usernameTextField.text ?? "",
                                              phoneNumber: self.phoneNumberTextField.text ?? "",
                                              gender: self.gender)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "tab")
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func logoutBarButtonTapped(_ sender: UIButton) {
        // Remove firebase data
        let firebaseAuth = Auth.auth()
        do { try firebaseAuth.signOut() }
        catch let signOutError as NSError { print ("Error signing out: %@", signOutError) }
        
        // logout from facebook and google
        GIDSignIn.sharedInstance().signOut()
        FBSDKLoginManager().logOut()
        
        if self.view.window?.rootViewController is LandingViewController {
            self.view.window?.rootViewController?.dismiss(animated: false, completion: {
                // Remove cached data and logout from system
            })
            return
        }
        
        let gateStoryboard = UIStoryboard(name: "Gate", bundle: nil)
        let landingViewController = gateStoryboard.instantiateViewController(withIdentifier: "landing")
        self.view.window?.rootViewController = landingViewController
        self.view.window?.rootViewController?.dismiss(animated: false, completion: {
            // Remove cached data and logout from system
        })
    }
}
