//
//  SigninViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/8/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import CoreLocation
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import FirebaseAuth
import GoogleSignIn
import OneSignal

class SigninViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.text = "" //"ahmed7@gmail.com"
        passwordTextField.text = "" //@7medHussien"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    // MARK: - @IBActions
    
    @IBAction func signinButtonTapped(_ sender: UIButton) {
        
        sender.startWhiteAnimating()
        
        APIManager.defaultManager.signin(withEmail: emailTextField.text ?? "",
                                         password: passwordTextField.text ?? "")
        {[weak self] (message, success) in
            sender.stopAnimating()
            
            if success {
                self?.presentHome()
                return
            }
            self?.showErrorAlert(withMessage: message ?? "")
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loginWithFacebook(_ sender: UIButton) {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile], viewController: self)
        {[weak self] (loginResult) in
            switch loginResult {
            case .failed(let error):
                DispatchQueue.main.async { self?.showErrorAlert(withMessage: error.localizedDescription) }
            case .cancelled:
                print("User cancelled login.")
            case .success(_, _, let accessToken):
                DispatchQueue.main.async {
                    self?.getFacebookGraph(accessToken)
                    self?.view.startOpequeLoadingView()
                }
            }
        }
    }
    
    @IBAction func setCredintialsBtnAct(_ sender: Any) {
        //
        passwordTextField.text = "" //@7medHussien"
        self.emailTextField.text = "ahmed7@gmail.com"
        self.passwordTextField.text = "@7medHussien"
    }
    @IBAction func loginWithGoogle(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK:- Private Helpers
    
    private func getFacebookGraph(_ accessToken: AccessToken){
        APIManager.defaultManager.getFaceBookUserData(accessToken) {[weak self]
            (fullName, email, credential, success, message) in
            
            if success {
                DispatchQueue.main.async { self?.loginWithFacebookCredential(fullName!, email!, credential!) }
                return
            }
            DispatchQueue.main.async { self?.showErrorAlert(withMessage: message ?? "") }
        }
    }
    
    private func loginWithFacebookCredential(_ fullName: String,_ email: String,_ credential: AuthCredential){
        APIManager.defaultManager.signin(withEmail: email, name: fullName, username: email, credential: credential) {
            [weak self] (message, success) in
            
            self?.view.stopOpequeLoadingView()
            if success {
                self?.presentHome()
                return
            }
            self?.showErrorAlert(withMessage: message ?? "")
            
        }
    }
    
    fileprivate func presentHome(){

        OneSignal.setSubscription(true)
        OneSignal.sendTag("userId", value: Auth.auth().currentUser!.uid)

        if CURRENT_USER?.phoneNumber == nil || CURRENT_USER?.gender == nil {
            let storyboard = UIStoryboard(name: "Gate", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "siginupInfo")
            (vc as? CompleteProfileViewController)?.uuid = CURRENT_USER?.userID
            self.present(vc, animated: true, completion: nil)
            return
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "tab")
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension SigninViewController: GIDSignInUIDelegate, GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Set user data
            guard let authentication = user.authentication else { return }
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                           accessToken: authentication.accessToken)
            let email = user.profile.email ?? ""
            let fullname = (user.profile.name ?? "")
            
            self.view.startOpequeLoadingView()
            
            // Login in firebase
            APIManager.defaultManager.signin(withEmail: email, name: fullname, username: email, credential: credential) {
                (message, success) in
                
                self.view.stopOpequeLoadingView()
                if success {
                    self.presentHome()
                    return
                }
                self.showErrorAlert(withMessage: message ?? "")
            }
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        self.showErrorAlert(withMessage: error.localizedDescription)
    }
}
