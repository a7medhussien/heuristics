//
//  LoadingViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 6/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoadingViewController: UIViewController {

    @IBOutlet weak var logoStackView: UIStackView!
    @IBOutlet weak var mainImgView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    
    var firstTimeFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidLayoutSubviews() {
        if firstTimeFlag == false {
            firstTimeFlag = true
            setupView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupAnimations()
    }
    
    func configData() {
        if let uid = Auth.auth().currentUser?.uid  {
            APIManager.defaultManager.getUser(uid: uid, completionHandler: {[weak self] (user, success, message) in
                if success && user != nil {
                    CURRENT_USER = user
                    self?.presentHome()
                }
            })
        }
    }
    
    fileprivate func presentHome(){
        
        if CURRENT_USER?.phoneNumber == nil || CURRENT_USER?.gender == nil {
            let storyboard = UIStoryboard(name: "Gate", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "siginupInfo")
            (vc as? CompleteProfileViewController)?.uuid = CURRENT_USER?.userID
            self.present(vc, animated: true, completion: nil)
            return
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "tab")
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func setupView() {
        let viewPosition = self.view.center.y - logoStackView.center.y
        logoStackView.transform = CGAffineTransform(translationX: 0, y: viewPosition)
        stackView.transform = CGAffineTransform(translationX: 0, y: 400)
    }
    
    func setupAnimations() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            UIView.animate(withDuration: 1, animations: { [weak self] in
                self?.logoStackView.transform = CGAffineTransform.identity
                self?.stackView.transform = CGAffineTransform.identity
            }, completion: {[weak self] (success) in
                self?.configData()
            })
        })
    }
}
