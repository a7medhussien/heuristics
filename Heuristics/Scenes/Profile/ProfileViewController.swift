//
//  ProfileViewController
//  Heuristics
//
//  Created by Ahmed Hussien on 2/8/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class ProfileViewController: HeuristicsBaseViewController {
    
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var maleFlagImgView: UIImageView!
    @IBOutlet weak var femaleFlagImgView: UIImageView!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!

    var user:User?
    var gender: Int = 1
    
    // MARK:- Navigation attribute
    fileprivate var transitionPoint: CGPoint!
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        self.configUI()
        self.configData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.configUI()
    }
    
    // MARK:- @IBAction
    
    @IBAction func followingTapped(_ sender: UIButton) {
        let followingViewController = FollowViewController(nibName: "FollowViewController", bundle: nil)
        followingViewController.type = .following
        let navBar = BaseNavigationViewController(rootViewController: followingViewController)
        self.navigationController?.present(navBar, animated: true, completion: nil)
    }
    
    @IBAction func followersTapped(_ sender: UIButton) {
        let followersViewController = FollowViewController(nibName: "FollowViewController", bundle: nil)
        followersViewController.type = .followers
        let navBar = BaseNavigationViewController(rootViewController: followersViewController)
        self.navigationController?.present(navBar, animated: true, completion: nil)
    }
    
    
    
    @IBAction func updateProfileTapped(_ sender: UIButton) {
        //let editProfileVC = EditProfileViewController(nibName: "EditProfileViewController", bundle: nil)
        //self.navigationController?.present(editProfileVC, animated: true, completion: nil)
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        guard nameTextField.validator.isNonEmpty() == nil, usernameTextField.validator.isNonEmpty() == nil else {
            self.showEmptyWarning()
            return
        }
        
        if let uid = CURRENT_USER?.userID {
            let name = nameTextField.text ?? ""
            let username = usernameTextField.text ?? ""
            
            CURRENT_USER?.name = name
            CURRENT_USER?.userName = username
            
            let firebaseDatabase = Database.database().reference()
            firebaseDatabase.child("users/\(uid)/Name").setValue(name)
            firebaseDatabase.child("users/\(uid)/username").setValue(username)
        }
        
        (self.tabBarController as? BaseTabBarViewController)?.changeTabBarIndex(index: 0)
        
    }
    
    @IBAction func addUserButtonTaoed(_ sender: UIButton) {
        let addUser = UsersListViewController()
        let nav = BaseNavigationViewController(rootViewController: addUser)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func maleGestureTapped(_ sender: UITapGestureRecognizer) {
        self.gender = 1
        self.profileImgView.image = #imageLiteral(resourceName: "avatar1")
        self.maleFlagImgView.image = #imageLiteral(resourceName: "tick")
        self.femaleFlagImgView.image = UIImage()
    }
    
    @IBAction func femaleGestureTapped(_ sender: UITapGestureRecognizer) {
        self.gender = 0
        self.profileImgView.image = #imageLiteral(resourceName: "avatar0")
        self.maleFlagImgView.image = UIImage()
        self.femaleFlagImgView.image = #imageLiteral(resourceName: "tick")
    }
    
    // MARK: - Private Helpers
    
    fileprivate func configUI(){
        
        if let uid = Auth.auth().currentUser?.uid, CURRENT_USER == nil  {
            APIManager.defaultManager.getUser(uid: uid, completionHandler: {[unowned self] (user, success, message) in
                if success && user != nil {
                    CURRENT_USER =  user
                    self.configUI()
                }
            })
        }
        else {
            
            self.user = CURRENT_USER
            self.fullNameLabel.text = user?.name ?? ""
            self.usernameLabel.text = user?.userName ?? ""
            self.followersLabel.text = "\(user?.followers.count ?? 0)"
            self.followingLabel.text = "\(user?.following.count ?? 0)"
            self.profileImgView.loadImage(user?.imageUrl ?? "")
            
            self.nameTextField.text = self.user?.name ?? ""
            self.usernameTextField.text = self.user?.userName ?? ""
            self.phoneNumber.text = user?.phoneNumber ?? ""
            self.gender = user?.gender ?? 1
            
            if gender == 0 {
                self.profileImgView.image = #imageLiteral(resourceName: "avatar0")
                self.maleFlagImgView.image = UIImage()
                self.femaleFlagImgView.image = #imageLiteral(resourceName: "tick")
            }
        }
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .save, target: self,
                                           action: #selector(doneTapped(_:)))
        self.navigationItem.rightBarButtonItem = doneButton
        
        let addUsersButton = UIBarButtonItem(image: #imageLiteral(resourceName: "add-friend"), style: .plain, target: self,
                                             action: #selector(addUserButtonTaoed(_:)))
        self.navigationItem.leftBarButtonItem = addUsersButton
    }
    
    func configData() {
        getFollowerUsers()
        getFollowingUsers()
    }
    
    func getFollowerUsers() {
        APIManager.defaultManager.getFollowers { (followers, success, message) in
            self.followersLabel.text = "\(CURRENT_USER?.followers.count ?? 0)"
        }
    }
    
    func getFollowingUsers() {
        APIManager.defaultManager.getFollowing { (following, success, message) in
            self.followingLabel.text = "\(CURRENT_USER?.following.count ?? 0)"
        }
    }
    
    fileprivate func completeProfileIfNeeded(){
        if self.user?.userName == nil || self.user?.userName == "" {
            //let editProfileVC = EditProfileViewController(nibName: "EditProfileViewController", bundle: nil)
            //self.navigationController?.present(editProfileVC, animated: true, completion: nil)
        }
    }
    
    func showEmptyWarning(){
        self.showAlertWithTitle(title: "Complete empty fields",
                                message: "In order to complete your operation you must fill all fields")
    }
    
    func compeleteProfileWarining() {
        self.showAlertWithTitle(title: "Complete your profile",
                                message: "In order to use the app you should complete your profile by fill all empty files and click done to save the new data")
    }
}

