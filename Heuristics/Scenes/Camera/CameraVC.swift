 //
 //  Heuristics
 //
 //  Created by Ahmed Zaghloul on 11/25/17.
 //  Copyright © 2017 HeuristicGang. All rights reserved.
 //

import UIKit

class CameraVC: SwiftyCamViewController, SwiftyCamViewControllerDelegate {
    
    @IBOutlet weak var captureButton: SwiftyRecordButton!
    @IBOutlet weak var flipCameraButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    
    var fullView: CGFloat {
        return UIScreen.main.bounds.height
    }
    var partialView: CGFloat {
        return UIScreen.main.bounds.height - (UIScreen.main.bounds.height * 0.3)
    }
    
	override func viewDidLoad() {
		super.viewDidLoad()
		cameraDelegate = self
		maximumVideoDuration = 10.0
//        shouldUseDeviceOrientation = true
//        allowAutoRotate = true
        audioEnabled = true
        
        constructPanGesture()
	}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        captureButton.delegate = self
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.6, animations: { [weak self] in
                let frame = self?.view.frame
//                let yComponent = self?.partialView
                self?.view.frame = CGRect(x: 0, y: 0, width: frame!.width, height: frame!.height)
            })
            
        }
    }
    
    func constructPanGesture()  {
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(CameraVC.panGesture(_:)))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
        
    }

	override var prefersStatusBarHidden: Bool {
		return true
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
		let newVC = PhotoViewController(image: photo)
		self.present(newVC, animated: true, completion: nil)
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
		print("Did Begin Recording")
		captureButton.growButton()
		UIView.animate(withDuration: 0.25, animations: {
			self.flashButton.alpha = 0.0
			self.flipCameraButton.alpha = 0.0
		})
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
		print("Did finish Recording")
		captureButton.shrinkButton()
		UIView.animate(withDuration: 0.25, animations: {
			self.flashButton.alpha = 1.0
			self.flipCameraButton.alpha = 1.0
		})
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
		let newVC = VideoViewController(videoURL: url)
		self.present(newVC, animated: true, completion: nil)
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
		let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
		focusView.center = point
		focusView.alpha = 0.0
		view.addSubview(focusView)

		UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
			focusView.alpha = 1.0
			focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
		}, completion: { (success) in
			UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations: {
				focusView.alpha = 0.0
				focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
			}, completion: { (success) in
				focusView.removeFromSuperview()
			})
		})
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
		print(zoom)
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
		print(camera)
	}
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error) {
        print(error)
    }

    @IBAction func cameraSwitchTapped(_ sender: Any) {
        switchCamera()
    }
    
    @IBAction func toggleFlashTapped(_ sender: Any) {
        flashEnabled = !flashEnabled
        
        if flashEnabled == true {
            flashButton.setImage(#imageLiteral(resourceName: "flash_button_selected"), for: UIControlState())
        } else {
            flashButton.setImage(#imageLiteral(resourceName: "flash_button"), for: UIControlState())
        }
    }
    
    @IBAction func closeTapped(){
        self.dismissVC(completion: nil)
    }
}
 
//Gesture Utils
 extension CameraVC {
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        
        let y = self.view.frame.minY
        self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
        recognizer.setTranslation(CGPoint.zero, in: self.view)
        
        print(velocity.y)
        print(y)
        
        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((partialView - y) / velocity.y )
            
            duration = duration > 1.3 ? 1 : duration
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                print(velocity.y)
                if  velocity.y >= 0  {
                    if velocity.y > 150 && y <= (self.partialView){
                        self.dismissVC(completion: nil)
                    }else{
                        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    }
                } else {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                }
                
            }, completion: { [weak self] _ in
                if y >= (self?.partialView)!  {
                    UIView.animate(withDuration: duration, animations: {
                        self?.view.frame = CGRect(x: 0, y: 0, width: (self?.view.frame.width)!, height: (self?.view.frame.height)!)
                    }, completion: { (_) in
                        self?.view.removeFromSuperview()
                    })
                }
            })
        
        }
    }
    
    func prepareBackgroundView(){
        let blurEffect = UIBlurEffect.init(style: .dark)
        let visualEffect = UIVisualEffectView.init(effect: blurEffect)
        let bluredView = UIVisualEffectView.init(effect: blurEffect)
        bluredView.contentView.addSubview(visualEffect)
        visualEffect.frame = UIScreen.main.bounds
        bluredView.frame = UIScreen.main.bounds
        view.insertSubview(bluredView, at: 0)
    }

 }
