//
//  FollowTableViewCell.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/11/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

class FollowTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var followButton: DesignableButton!
    
}
