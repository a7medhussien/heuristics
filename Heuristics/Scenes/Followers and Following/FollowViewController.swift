//
//  FollowViewController.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/11/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import Kingfisher

class FollowViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var type: followViewType = .followers
    var followUsers = [FollowModel?]()
    fileprivate let followCellReuseIdentifier = "FollowTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        getData()
    }
    
    func getData() {
        switch type {
        case .following:
            getFollowingUsers()
            self.title = "Following"
        case .followers:
            getFollowerUsers()
            self.title = "Followers"
        }
    }
    
    func getFollowerUsers() {
        APIManager.defaultManager.getFollowers { (followers, success, message) in
            self.followUsers = followers
            self.tableView.reloadData()
        }
    }
    
    func getFollowingUsers() {
        APIManager.defaultManager.getFollowing { (following, success, message) in
            self.followUsers = following
            self.tableView.reloadData()
        }
    }
    
    func configTableView() {
        let closeButton = UIBarButtonItem(title: "Close", style: .plain, target: self,
                                          action: #selector(self.closeTapped(_:)))
        self.navigationItem.leftBarButtonItem = closeButton
        
        let followCell = UINib(nibName: followCellReuseIdentifier, bundle: Bundle.main)
        self.tableView.register(followCell, forCellReuseIdentifier: followCellReuseIdentifier)
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func followTapped(_ sender: UIButton) {
        switch type {
        case .following:
            APIManager.defaultManager.unfollow(withUid: followUsers[sender.tag]?.userID)
        case .followers:
            if checkFollowState(withUid: followUsers[sender.tag]?.userID ?? "") {
                APIManager.defaultManager.unfollow(withUid: followUsers[sender.tag]?.userID)
                sender.unfollow()
                return
            }
            sender.startAnimating()
            APIManager.defaultManager.follow(withUid: followUsers[sender.tag]?.userID) {[weak self] (message, success) in
                if success { sender.follow() }
                else { self?.showErrorAlert(withMessage: message ?? "") }
                sender.stopAnimating()
            }
        }
    }
}

extension FollowViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}

extension FollowViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return followUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: followCellReuseIdentifier, for: indexPath) as! FollowTableViewCell
        cell.nameLabel.text = followUsers[indexPath.row]?.name ?? ""
        cell.usernameLabel.text = followUsers[indexPath.row]?.userName ?? ""
        let placeHolder = followUsers[indexPath.row]?.gender == 0 ? #imageLiteral(resourceName: "avatar0") : #imageLiteral(resourceName: "avatar1")
        cell.userImgView.loadImage(followUsers[indexPath.row]?.imageUrl ?? "", placeHolder)
        cell.followButton.tag = indexPath.row
        type == .followers ? cell.followButton.unfollow() : cell.followButton.follow()
        if checkFollowState(withUid: followUsers[indexPath.row]?.userID ?? "") {
            cell.followButton.follow()
        }
        cell.followButton.addTarget(self, action:#selector(self.followTapped), for: UIControlEvents.touchUpInside)
        cell.followButton.tag = indexPath.row
        return cell
    }
}
