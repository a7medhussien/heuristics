//
//  FollowModel.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/20/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import Foundation
import ObjectMapper

class FollowModel: User {
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override init(){
        super.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        name <- map["name"]
        userID <- map["userID"]
    }
    
}
