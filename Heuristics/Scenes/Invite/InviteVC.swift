//
//  InviteVC.swift
//  Heuristics
//
//  Created by Magdy Zamel on 4/18/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Contacts
import MessageUI

class InviteVC: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource,MFMessageComposeViewControllerDelegate ,PhoneCell {

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(InviteVC.handleRefresh), for: UIControlEvents.valueChanged)
        return refreshControl
    }()

    @IBOutlet weak var Contacts: UITableView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey] as [Any]

        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }

        var results: [CNContact] = []

        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)

            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }

        Singleton.UserContacts = [[String : String]]()
        for contact in results {
            if contact.phoneNumbers.count >= 1{
                let name = contact.givenName + " " + contact.familyName
                let phone = contact.phoneNumbers[0].value.stringValue
                Singleton.UserContacts.append(["name": name, "phone" : phone])

            }
        }

        Singleton.UserContacts = Singleton.UserContacts.sorted { $0["name"]!.localizedCaseInsensitiveCompare($1["name"]!) == ComparisonResult.orderedAscending }



    }

    override func viewDidLoad() {
        super.viewDidLoad()
        Contacts.reloadData()
        Contacts.refreshControl = refreshControl


    }

    func handleRefresh(refreshControl: UIRefreshControl) {


        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey] as [Any]

        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }

        var results: [CNContact] = []

        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)

            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }

        Singleton.UserContacts = [[String : String]]()
        for contact in results {
            if contact.phoneNumbers.count >= 1{
                let name = contact.givenName + " " + contact.familyName
                let phone = contact.phoneNumbers[0].value.stringValue
                Singleton.UserContacts.append(["name": name, "phone" : phone])

            }
        }

        Singleton.UserContacts = Singleton.UserContacts.sorted { $0["name"]!.localizedCaseInsensitiveCompare($1["name"]!) == ComparisonResult.orderedAscending }



        refreshControl.endRefreshing()

    }



    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "FRIENDS")
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return Singleton.UserContacts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell : contact = tableView.dequeueReusableCell(withIdentifier: "contact") as! contact

        cell.username.text = Singleton.UserContacts[indexPath.row]["name"]
        cell.delegate = self
        cell.phone.text = Singleton.UserContacts[indexPath.row]["phone"]

        return cell
    }

    func PhoneCellTaped(name:String,phone:String) {

        let messageVC = MFMessageComposeViewController()

        messageVC.body = "Hey \(name)! \nstay safe, join Heuristics and create your real siocal Family and share your feedback with them and enjoying with nearby offers \n Get Heuristics now https://heuristics.me/";
        messageVC.recipients = [phone]
        messageVC.messageComposeDelegate = self;
        self.present(messageVC, animated: true, completion: nil)
    }


    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {


        switch result {
        case .cancelled:
            print("Message was failed")
        case .failed:
            print("Message was failed")
        case .sent:
            print("Message was sent")
        }
        self.dismiss(animated: true, completion: nil)

    }

}

protocol PhoneCell:class {
    func PhoneCellTaped(name:String,phone:String)
}

class contact : UITableViewCell {

    weak var delegate : PhoneCell!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var username : UILabel!
    @IBOutlet weak var phone : UILabel!

    @IBAction func invite(_ sender: UIButton) {
        delegate.PhoneCellTaped(name: username.text!, phone: phone.text!)
    }

}




