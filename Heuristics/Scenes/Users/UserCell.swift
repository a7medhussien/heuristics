//
//  UserCell.swift
//  Locationapp
//
//  Created by Mostafa El_sayed on 11/25/17.
//  Copyright © 2017 HeuristicGang. All rights reserved.
//

import UIKit




class UserCell: UITableViewCell {
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var date: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
