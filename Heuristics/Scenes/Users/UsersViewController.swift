 //
//  MessagesViewController.swift
//  Locationapp
//
//  Created by Mostafa El_sayed on 11/25/17.
//  Copyright © 2017 HeuristicGang. All rights reserved.
//

import UIKit
import XLPagerTabStrip
 
class UsersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    lazy var users: [User] = [User]()
    lazy var usersFiltered: [User] = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTable()
        loadUsers()
    }
    

    // MARK: - @IBActions

    
    // MARK - Helper methods
    fileprivate func loadUsers() {
        APIManager.defaultManager.getUsers { (users, status, msg) in



            if status {
                
                self.users = users!
                self.usersFiltered = self.users
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
    fileprivate func setupTable() {
        tableView.delegate = self
        tableView.dataSource = self
        
        let backgorundView = UIView()
        backgorundView.backgroundColor = .mainAppColor
        tableView.tableFooterView = backgorundView
         
        let cellCategoryNib = UINib(nibName: "UserCell", bundle: nil)
        tableView.register(cellCategoryNib, forCellReuseIdentifier: "UserCell")
    }

    
}
extension UsersViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersFiltered.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
       
        cell.accessoryType = .none
        
        cell.senderName.text! = usersFiltered[indexPath.row].name ?? ""
        cell.date.text! = usersFiltered[indexPath.row].email  ?? ""
        return cell
    }
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath) as! UserCell
        cell.accessoryType = .none
        let vc:ChatVC = ChatSectionStoryboard.chat.viewController()
        vc.title = users[indexPath.row].name!
        vc.receiverName = users[indexPath.row].name!
        vc.ownerId = users[indexPath.row].userID!
        vc.receiverId = users[indexPath.row].userID!

        self.pushVC(vc)
    }
}
 
 // MARK: - IndicatorInfoProvider
 extension UsersViewController: IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "People"
    }
 }




