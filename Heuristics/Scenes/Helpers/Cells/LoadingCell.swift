//
//  LoadingCell.swift
//  Edfa3ly
//
//  Created by Mostafa El_sayed on 10/11/17.
//  Copyright © 2017 Robusta. All rights reserved.
//

import UIKit

protocol LoadingView {
    func display(loadingMessage: String)
}
class LoadingCell: UITableViewCell, LoadingView {

    @IBOutlet weak var loadingTitle: UILabel!
    func display(loadingMessage: String) {
        self.loadingTitle.text! = loadingMessage
    }
    
}
