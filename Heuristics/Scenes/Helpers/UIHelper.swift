//
//  UIHelper.swift
//  HeuristicsOffers
//
//  Created by Magdy Zamel on 8//5/17.
//  Copyright © 2017 Magdy Zamel. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation

class UiHelpers {
    class func showLoader() {
        let activityData = ActivityData( type: NVActivityIndicatorType.ballClipRotate)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }

    class func hideLoader() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    class func checkingLocationServicesEnabledAlertOn(viewController:UIViewController) {

        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                let alertController = UIAlertController (title: "Failure", message: "We would like to use your location. \n Please enable location services from Setting > LocationServices. ", preferredStyle: .alert)

                let settingsAction = UIAlertAction(title: "Goto HeuristicsOffers Settings", style: .default) { (_) -> Void in

                    UIApplication.shared.openURL(URL(string : "\(UIApplicationOpenSettingsURLString)path=HeuristicsOffers/LOCATION_SERVICES")!)
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Later", style: .default, handler: nil)
                alertController.addAction(cancelAction)
                DispatchQueue.main.async(execute: {
                    viewController.present(alertController, animated: true, completion: nil)
                })
            case .authorizedAlways, .authorizedWhenInUse:
                return
            }
        }
    }

}

class LoaderIndicatorManager{

    class func show() {
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }

    class func hide() {
//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }

}



extension UIImage {

    func colored(with color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: size)
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }

}


