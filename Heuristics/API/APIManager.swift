//
//  APIManager.swift
//  Heuristics
//
//  Created by Mostafa El_sayed on 8/29/17.
//  Copyright © 2017 TheGang. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SwiftyJSON
import Kingfisher
import ObjectMapper
import FBSDKLoginKit
import FacebookCore
import FacebookLogin
import OneSignal
import CoreLocation

class APIManager {
    
    static let defaultManager = APIManager()
    private let apiBaseUrl = ""
    private let googleApiKey = "AIzaSyBgCJ21KKJMeMcO8s3DRCYKTAjAl8XtvfQ"
    lazy private var firebaseDatabase = Database.database().reference()
    private var userID: String? {
        get { return Auth.auth().currentUser?.uid }
    }
    
    
    private init (){}
    
    //MARK:- Api configures
    
    typealias CompletionHandler = (_ message: String?, _ success: Bool) -> Void
    typealias RegCompletionHandler = (_ message: String?, _ uid: String?, _ success: Bool) -> Void
    typealias GetNearbyPlacesCompletionHandler = (_ places: [GooglePlace]?, _ success: Bool, _ message: String) -> Void
    typealias GetUsersCompletionHandler = (_ places: [User]?, _ success: Bool, _ message: String) -> Void
    typealias GetUserCompletionHandler = (_ places: User?, _ success: Bool, _ message: String?) -> Void
    typealias GetReviewsCompletionHandler = (_ places: [HeuReview]?, _ success: Bool, _ message: String) -> Void
    typealias GetMessagesCompletionHandler = (_ places: [Geotification]?, _ success: Bool, _ message: String) -> Void
    typealias GetFollowUsers = (_ followUsers: [FollowModel?], _ success: Bool, _ message: String?) -> Void
    typealias GetFaceBookUserData = (_ fullName: String?,_ email: String?,_ credential: AuthCredential?,
        _ success: Bool, _ message: String?) -> Void
    typealias FlagCompletionHandler = (_ flag: Bool?, _ message: String?, _ success: Bool) -> Void

    enum httpMethod: String {
        case post = "POST"
        case get = "GET"
        case delete = "DELETE"
    }
    
    // MARK:- Gate
    
    func signin(withEmail email: String, password: String, completionHandler: @escaping CompletionHandler) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            
            if let error = error {
                completionHandler(error.localizedDescription, false); return
            }

            OneSignal.setSubscription(true)
            OneSignal.sendTag("userId", value: user!.uid)

            self.getUser(uid: user?.uid ?? "", completionHandler: { (user, success, message) in
                if success {
                    CURRENT_USER = user
                    completionHandler(nil, true)
                } else { completionHandler(message, false) }
            })
        }
    }
    
    func signin(withEmail email: String, name: String, username: String, credential: AuthCredential,
                completionHandler: @escaping CompletionHandler){
        
        Auth.auth().signIn(with: credential) {[weak self] (user, error) in
            if let error = error { completionHandler(error.localizedDescription, false); return }

            OneSignal.setSubscription(true)
            OneSignal.sendTag("userId", value: user!.uid)


            self?.setUserData(newEmail: email, newName: name, newUsername: username, uid: user?.uid ?? "", completionHandler:{ (_, success, message) in
                
                if success { completionHandler(nil, true) }
                else{ completionHandler(message, false) }
                
            })
        }
    }
    
    func getFaceBookUserData(_ accessToken: AccessToken, completionHandler: @escaping GetFaceBookUserData){
        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,first_name,last_name"], tokenString: accessToken.authenticationToken, version: nil, httpMethod: "GET")
        
        _ = req?.start(completionHandler: { (connection, resultResponse, error) in
            if let result = resultResponse as? NSDictionary {
                let fullName = (result.value(forKey: "first_name") as? String ?? "") + " " +
                    (result.value(forKey: "last_name") as? String ?? "")
                let email = result.value(forKey: "email") as? String ?? ""
                
                let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.authenticationToken)
                
                completionHandler(fullName, email, credential, true, nil)
            }
            else {
                completionHandler(nil, nil, nil, false, error?.localizedDescription ?? "")
            }
        }).start()
    }
    
    func signup(withEmail email: String, password: String,
                completionHandler: @escaping RegCompletionHandler){
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard error == nil, let uid = user?.uid else {
                completionHandler(error?.localizedDescription ?? "", nil, false); return
            }

            OneSignal.setSubscription(true)
            OneSignal.sendTag("userId", value: uid)
            
            completionHandler(nil, uid, true)
        }
    }
    
    func setUserData(withUid uid: String, email: String,
                     name: String, username: String,
                     phoneNumber: String, gender: Int)
    {
        let userModel = User()
        userModel.email = email
        userModel.name = name
        userModel.userName = username
        userModel.phoneNumber = phoneNumber
        userModel.gender = gender
        userModel.userID = uid
        CURRENT_USER = userModel
        
        self.firebaseDatabase.child("users/\(uid)/userId").setValue(uid)
        self.firebaseDatabase.child("users/\(uid)/Email").setValue(userModel.email)
        self.firebaseDatabase.child("users/\(uid)/Name").setValue(userModel.name)
        self.firebaseDatabase.child("users/\(uid)/username").setValue(userModel.userName)
        self.firebaseDatabase.child("users/\(uid)/phoneNumber").setValue(userModel.phoneNumber)
        self.firebaseDatabase.child("users/\(uid)/gender").setValue(userModel.gender)
    }
    
    // MARK:- User
    
    func getUser(uid: String ,completionHandler: @escaping GetUserCompletionHandler){
        
        firebaseDatabase.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshotValue = snapshot.value  as? [String: Any] {
                
                let user = Mapper<User>().map(JSON: snapshotValue)
                
                for child in snapshotValue["followers"] as? [String: Any] ?? [:] {
                    let data = child.value as? [String: Any]
                    let followUser = Mapper<FollowModel>().map(JSON: data ?? [:])
                    user?.followers.append(followUser)
                }
                
                for child in snapshotValue["following"] as? [String: Any] ?? [:] {
                    let data = child.value as? [String: Any]
                    let followUser = Mapper<FollowModel>().map(JSON: data ?? [:])
                    user?.following.append(followUser)
                }
                
                user?.userID = uid
                completionHandler(user, true, nil)
                return
            }
            
            completionHandler(nil, false, "Something went wrong, try again")
            
        })  { (error) in completionHandler(nil, false, error.localizedDescription) }
    }
    
    private func setUserData(newEmail: String,newName: String,newUsername: String,uid: String,
                             completionHandler: @escaping GetUserCompletionHandler) {
        
        self.firebaseDatabase.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let snapshotValue = snapshot.value  as? [String: Any]
            
            let user = Mapper<User>().map(JSON: snapshotValue ?? [:])
            
            for child in snapshotValue?["followers"] as? [String: Any] ?? [:] {
                let data = child.value as? [String: Any]
                let followUser = Mapper<FollowModel>().map(JSON: data ?? [:])
                user?.followers.append(followUser)
            }
            
            for child in snapshotValue?["following"] as? [String: Any] ?? [:] {
                let data = child.value as? [String: Any]
                let followUser = Mapper<FollowModel>().map(JSON: data ?? [:])
                user?.following.append(followUser)
            }
            
            user?.userID = uid
            if user?.email == nil { user?.email = newEmail }
            if user?.name == nil { user?.name = newName }
            if user?.userName == nil { user?.userName = newUsername }
            
            CURRENT_USER = user
            
            self.firebaseDatabase.child("users/\(uid)/userId").setValue(uid)
            self.firebaseDatabase.child("users/\(uid)/Email").setValue(user?.email ?? "")
            self.firebaseDatabase.child("users/\(uid)/Name").setValue(user?.name ?? "")
            self.firebaseDatabase.child("users/\(uid)/username").setValue(user?.userName ?? "")
            
            completionHandler(user, true, nil)
            
        }) { (error) in completionHandler(nil, false, error.localizedDescription) }
        
    }
    
    func getFollowers(completionHandler: @escaping GetFollowUsers){
        
        var followers = [FollowModel?]()
        firebaseDatabase.child("users").child(userID ?? "").child("followers").observe(.value, with:
            { (snapshot) in
                followers = []
                for child in snapshot.children {
                    let snapshot = child  as! DataSnapshot
                    let snapshotValue = snapshot.value  as! [String: Any]
                    let follower = Mapper<FollowModel>().map(JSON: snapshotValue)
                    followers.append(follower)
                }
                CURRENT_USER?.followers = followers
                completionHandler(followers, true, nil)
        })
        
    }
    
    func getFollowing(completionHandler: @escaping GetFollowUsers){
        
        var following = [FollowModel?]()
        firebaseDatabase.child("users").child(CURRENT_USER?.userID ?? "").child("following").observe(.value, with:
            { (snapshot) in
                following = []
                for child in snapshot.children {
                    let snapshot = child  as! DataSnapshot
                    let snapshotValue = snapshot.value  as! [String: Any]
                    let follower = Mapper<FollowModel>().map(JSON: snapshotValue)
                    following.append(follower)
                }
                CURRENT_USER?.following = following
                completionHandler(following, true, nil)
        })
        
    }
    
    func follow(withUid uid: String?, completionHandler: @escaping CompletionHandler){
        
        let currentUserUid = CURRENT_USER?.userID ?? ""
        self.getUser(uid: uid ?? "" , completionHandler: { (user, success, message) in
            
            if success
            {
                //Following
                self.firebaseDatabase
                    .child("users/\(currentUserUid)/following/\(uid ?? "")/name")
                    .setValue(user?.name ?? "")
                self.firebaseDatabase
                    .child("users/\(currentUserUid)/following/\(uid ?? "")/username")
                    .setValue(user?.userName ?? "")
                self.firebaseDatabase
                    .child("users/\(currentUserUid)/following/\(uid ?? "")/userID")
                    .setValue(user?.userID ?? "")
                self.firebaseDatabase
                    .child("users/\(currentUserUid)/following/\(uid ?? "")/imageURL")
                    .setValue(user?.imageUrl ?? "")
                self.firebaseDatabase
                    .child("users/\(currentUserUid)/following/\(uid ?? "")/gender")
                    .setValue(user?.gender ?? "")
                
                //Followers
                self.firebaseDatabase
                    .child("users/\(uid ?? "")/followers/\(currentUserUid)/name")
                    .setValue(CURRENT_USER?.name ?? "")
                self.firebaseDatabase
                    .child("users/\(uid ?? "")/followers/\(currentUserUid)/username")
                    .setValue(CURRENT_USER?.userName ?? "")
                self.firebaseDatabase
                    .child("users/\(uid ?? "")/followers/\(currentUserUid)/userID")
                    .setValue(CURRENT_USER?.userID ?? "")
                self.firebaseDatabase
                    .child("users/\(uid ?? "")/followers/\(currentUserUid)/imageURL")
                    .setValue(CURRENT_USER?.imageUrl ?? "")
                self.firebaseDatabase
                    .child("users/\(uid ?? "")/followers/\(currentUserUid)/gender")
                    .setValue(user?.gender ?? "")
                
                let userFollow = FollowModel()
                userFollow.name = user?.name
                userFollow.userName = user?.userName
                userFollow.userID = user?.userID
                userFollow.imageUrl = user?.imageUrl
                CURRENT_USER?.following.append(userFollow)
                
                completionHandler(nil, true)
            } else{ completionHandler(message, false) }
        })
    }
    
    func unfollow(withUid uid: String?){
        firebaseDatabase
            .child("users")
            .child(CURRENT_USER?.userID ?? "")
            .child("following")
            .child(uid ?? "").removeValue()
        
        firebaseDatabase
            .child("users")
            .child(uid ?? "")
            .child("followers")
            .child(CURRENT_USER?.userID ?? "").removeValue()
        
        var counter = 0
        for user in CURRENT_USER?.following ?? [] {
            if user?.userID == uid {
                CURRENT_USER?.following.remove(at: counter)
            }
            counter += 1
        }
    }
    
    //MARK:- Unknow
    
    func getNearbyPlaces(latitude: Double, longitude: Double, radius: Double, completionHandler:@escaping GetNearbyPlacesCompletionHandler) {
        
        
        let urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&type=store&radius=\(radius)&key=\(googleApiKey)"
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        Alamofire.request(request).responseData { (response) in
            
            if let error = response.error {
                print(error)
                completionHandler(nil, false, error.localizedDescription)
            }
            
            
            
            
            var placesArray: [GooglePlace] = []
            
            guard let data = response.data,
                let json = try? JSON(data: data, options: .mutableContainers),
                let results = json["results"].arrayObject as? [[String: Any]] else {
                    completionHandler(nil, false, "Failure")
                    return
                    
            }
            
            results.forEach {
                let place = GooglePlace(dictionary: $0)
                placesArray.append(place)
            }
            
            
            completionHandler(placesArray, true, "Places Feached")
            
        }
        
    }
    
    func getUsers(completion:@escaping GetUsersCompletionHandler){
        
        var users = [User]()
        firebaseDatabase.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children {
                
                if let snapshotValue = (child as? DataSnapshot)?.value  as? [String: Any] {
                    
                    if let user = Mapper<User>().map(JSON: snapshotValue) {
                        for child in snapshotValue["followers"] as? [String: Any] ?? [:] {
                            let data = child.value as? [String: Any]
                            let followUser = Mapper<FollowModel>().map(JSON: data ?? [:])
                            user.followers.append(followUser)
                        }
                        
                        for child in snapshotValue["following"] as? [String: Any] ?? [:] {
                            let data = child.value as? [String: Any]
                            let followUser = Mapper<FollowModel>().map(JSON: data ?? [:])
                            user.following.append(followUser)
                        }
                        
                        users.append(user)
                    }
                    
                }
                
            }
            print(users.description)
            completion(users,true,"success")
        }) { error in completion(nil, false, error.localizedDescription) }
        
    }
    
    func addReview(withReview review:HeuReview) {
        
        let userID : String = (Auth.auth().currentUser?.uid)!
        let username : String = "Heu Feedback User"
        review.authorName = username
        print("Current user ID is" + userID)
        
        self.firebaseDatabase.child("Feedbacks").child(review.placeID).childByAutoId().setValue([
            "userID" : userID,
            "review" : review.reviewContent,
            "rating" : review.rating,
            "placeID" : review.placeID,
            "time" :review.time,
            "authorName" : username
            ]
        )
        
    }
    
    func getReviews(withPlaceID placeID:String, completion:@escaping GetReviewsCompletionHandler){
        
        var reviews = [HeuReview]()
        
        firebaseDatabase.child("Feedbacks").observe(.value, with: { (snapshot) in
            reviews = [HeuReview]()
            for child in snapshot.children {
                let reviewsSnapShot = child  as! DataSnapshot
                let key = reviewsSnapShot.key
                if key != placeID {
                    continue
                }
                for reviewSnapShot in reviewsSnapShot.children {
                    let reviewSnapShotValue = reviewSnapShot  as! DataSnapshot
                    let reviewSnapShotValues = reviewSnapShotValue.value as! NSDictionary
                    
                    let authorName = reviewSnapShotValues["authorName"] as? String ?? "Heu User"
                    let rating = reviewSnapShotValues["rating"] as? Double ?? 1
                    let review = reviewSnapShotValues["review"] as? String ?? ""
                    let time = reviewSnapShotValues["time"] as? String ?? "\(Date())"
                    
                    let timeAgo = Date(dateString: time, format: "yyyy-MM-dd HH:mm:ss +zzzz").timeAgoSinceNow
                    
                    let reviewDic = [
                        "review" : review,
                        "rating" : rating,
                        "placeID" : placeID,
                        "authorName" :  authorName,
                        "time" : timeAgo
                        ] as [String : Any]
                    
                    let heuReview = HeuReview(dictionary: reviewDic)
                    reviews.append(heuReview)
                    
                }
            }
            completion(reviews,true,"success")
        })
        
    }
    
    func getUserSentMessages(withUserId userId:String, completion:@escaping GetMessagesCompletionHandler){
        
        var messages = [Geotification]()
        
        firebaseDatabase.child("location_messages").observe(.value, with: { (snapshot) in
            messages = [Geotification]()
            for child in snapshot.children {
                let messagesSnapShot = child  as! DataSnapshot
                
                
                
                for messageSnapShot in messagesSnapShot.children {
                    let messageSnapShotValue = messageSnapShot  as! DataSnapshot
                    let messageValues = messageSnapShotValue.value as! NSDictionary
                    let senderId = messageValues.getValueForKey(key: "sender_id", callback: "")
                    
                    if senderId != userId {
                        continue
                    }
                    
                    let type = EventType(rawValue: messageValues.getValueForKey(key: "eventType", callback: "On Entry"))!
                    let iden = messageValues.getValueForKey(key: "identifier", callback: "")
                    let lat = messageValues.getValueForKey(key: "lat", callback: 0.0)
                    let lng = messageValues.getValueForKey(key: "lng", callback: 0.0)
                    let co = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    let nt = messageValues.getValueForKey(key: "note", callback: "")
                    let rd = messageValues.getValueForKey(key: "radius", callback: 100.0)
                    let senderName = messageValues.getValueForKey(key: "sender_name", callback: "")
                    
                    self.getUser(uid: messagesSnapShot.key, completionHandler: { (receiverName, status, message) in
                        if status {
                            let geo = Geotification(coordinate: co, radius: rd, identifier: iden, note: nt, eventType: type,senderName: receiverName?.userName ?? receiverName?.email ?? senderName, senderId: receiverName?.userID ?? "")
                            messages.append(geo)
                            completion(messages,true,"success")
                        }
                    })
                    
                    
                }
            }
            
        })
        
    }
    
    func removeLocationDart(withDart dart: Geotification, handler: @escaping CompletionHandler) {
                let senderId = dart.senderId
            firebaseDatabase.child("location_messages").child(senderId).child(dart.identifier).removeValue(completionBlock: { (error, _) in
                    if error != nil {
                        
                        handler("Something went wrong with your connection, please try again", false)
                        return
                    }
                    handler("item deleted", true)
                })
    }
    
    func removeLocationReminder(withUserId userId: String, reminder: Geotification, handler: @escaping CompletionHandler) {
        firebaseDatabase.child("locationReminders").child(userId).child(reminder.identifier).removeValue(completionBlock: { (error, _) in
            if error != nil {
                
                handler("Something went wrong with your connection, please try again", false)
                return
            }
            handler("item deleted", true)
        })
    }
    
    func getUserReceivedMessages(withUserId userId:String, completion:@escaping GetMessagesCompletionHandler) {
        
        var messages = [Geotification]()
        
        firebaseDatabase.child("location_messages").observe(.value, with: { (snapshot) in
            messages = [Geotification]()
            for child in snapshot.children {
                
                let messagesSnapShot = child  as! DataSnapshot
                let key = messagesSnapShot.key
                if key != userId {
                    continue
                }
                
                
                for messageSnapShot in messagesSnapShot.children {
                    
                    let messageSnapShotValue = messageSnapShot  as! DataSnapshot
                    let messageValues = messageSnapShotValue.value as! NSDictionary
                    let senderId = messageValues.getValueForKey(key: "sender_id", callback: "")
                    
//                    if senderId != userId {
//                        continue
//                    }
                    let type = EventType(rawValue: messageValues.getValueForKey(key: "eventType", callback: "On Entry"))!
                    let iden = messageValues.getValueForKey(key: "identifier", callback: "")
                    let lat = messageValues.getValueForKey(key: "lat", callback: 0.0)
                    let lng = messageValues.getValueForKey(key: "lng", callback: 0.0)
                    let co = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    let nt = messageValues.getValueForKey(key: "note", callback: "")
                    let rd = messageValues.getValueForKey(key: "radius", callback: 100.0)
                    let sender = messageValues.getValueForKey(key: "sender_name", callback: "")
                    let geo = Geotification(coordinate: co, radius: rd, identifier: iden, note: nt, eventType: type,senderName:sender, senderId : senderId)
                    messages.append(geo)
                    
                }
            }
            completion(messages,true,"success")
        })
        
    }
    
    func getUserLocationReminders(withUserId userId:String, completion:@escaping GetMessagesCompletionHandler) {
        
        var messages = [Geotification]()
        
        firebaseDatabase.child("locationReminders").observe(.value, with: { (snapshot) in
            messages = [Geotification]()
            for child in snapshot.children {
                
                let messagesSnapShot = child  as! DataSnapshot
                let key = messagesSnapShot.key
                if key != userId {
                    continue
                }
                
                
                for messageSnapShot in messagesSnapShot.children {
                    
                    let messageSnapShotValue = messageSnapShot  as! DataSnapshot
                    let messageValues = messageSnapShotValue.value as! NSDictionary
                    let senderId = messageValues.getValueForKey(key: "sender_id", callback: "")
                    
                    //                    if senderId != userId {
                    //                        continue
                    //                    }
                    let type = EventType(rawValue: messageValues.getValueForKey(key: "eventType", callback: "On Entry"))!
                    let iden = messageValues.getValueForKey(key: "identifier", callback: "")
                    let lat = messageValues.getValueForKey(key: "lat", callback: 0.0)
                    let lng = messageValues.getValueForKey(key: "lng", callback: 0.0)
                    let co = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    let nt = messageValues.getValueForKey(key: "note", callback: "")
                    let rd = messageValues.getValueForKey(key: "radius", callback: 100.0)
                    let sender = messageValues.getValueForKey(key: "sender_name", callback: "")
                    let geo = Geotification(coordinate: co, radius: rd, identifier: iden, note: nt, eventType: type,senderName:sender, senderId : senderId)
                    messages.append(geo)
                    
                }
            }
            completion(messages,true,"success")
        })
        
    }
    
    func getPlaceDetails(placeID: String , completionHandler:@escaping (_ placeDetails: GooglePlaceDetails?, _ success: Bool, _ message: String) -> Void) {
        
        let urlString = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeID)&key=\(googleApiKey)"
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                guard let result = json["result"].dictionaryObject  else {
                    
                    completionHandler(nil, false, "Failure")
                    return
                }
                
                let placeDetails = GooglePlaceDetails(dictionary: result)
                completionHandler(placeDetails, true, "Place Details Feached")
                
            case .failure(let error):
                completionHandler(nil, false, error.localizedDescription)
                
            }
        }
    }
    
    func getPhotoLinkFromReference(_ reference: String) -> String {
        
        let urlString = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=\(reference)&key=\(googleApiKey)"
        
        return urlString
    }
    func postMessageWithBody(messageBody: MessageBodyNotification){
        let url  = "https://onesignal.com/api/v1/notifications"
        let httpHeaderFields =  ["Content-Type": "application/json","Authorization":"Basic YzEzMDI5NTctYjhlNi00MTExLTllODEtZDNjMWIzZTZkNjdk"]
        print(messageBody.toDic())
        Alamofire.request(url, method: .post, parameters: messageBody.toDic(), encoding: JSONEncoding.default, headers: httpHeaderFields)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)   // result of response serialization
        }
    }
}

