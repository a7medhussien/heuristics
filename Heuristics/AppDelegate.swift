//
//  AppDelegate.swift
//  Heuristics
//
//  Created by Ahmed Hussien on 2/7/18.
//  Copyright © 2018 Heuristic Gang. All rights reserved.
//

import UIKit

import Firebase
import IQKeyboardManagerSwift
import CoreLocation
import FacebookCore
import GoogleSignIn
import OneSignal
import FirebaseDatabase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let locationManager = CLLocationManager()
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // setup OneSignal With launchOptions
        if ReciverManger.shared == nil {
            ReciverManger.shared = ReciverManger()
        }
        setupOneSignalWith(launchOptions: launchOptions)
        // Status bar color
        UIApplication.shared.statusBarStyle = .default
        // Automatic keyboard view scrolling
        IQKeyboardManager.sharedManager().enable = true
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        // Initialize google sign-in
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        // open Initial ViewController
        openInitialViewController()
        
        // Configuare location
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()

        
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        AppEventsLogger.activate(application)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,annotation: annotation)
    }

    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {

        if url.absoluteString.contains("google") {
            let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
            let annotation = options[UIApplicationOpenURLOptionsKey.annotation]
            return GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,annotation: annotation)
        }else {
            return SDKApplicationDelegate.shared.application(app, open: url, options: options)
        }
    }
    private func openInitialViewController(){


        if let _ = Auth.auth().currentUser {
            OneSignal.setSubscription(true)
            let storyboard = UIStoryboard(name: "Gate", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "loading")
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }else{
        
        }
    }

    func setupOneSignalWith(launchOptions:[UIApplicationLaunchOptionsKey: Any]?){

        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in

        }

        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            let payload: OSNotificationPayload = result!.notification.payload

            if payload.additionalData != nil {
                if  let _ = payload.additionalData["type"]{
                        self.window?.rootViewController = ChatSectionStoryboard.initialViewController()
                        self.window?.makeKeyAndVisible()
                }
            }
        }

        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false,
                                     kOSSettingsKeyInAppLaunchURL: true]
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification


        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: Singleton.oneSignalAppId,
                                        handleNotificationReceived: notificationReceivedBlock,
                                        handleNotificationAction: notificationOpenedBlock,
                                        settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            if !accepted{
                let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
                let userID = status.subscriptionStatus.userId
                print("userID = \(userID)")
            }
        })
    }
    
    // MARK: - Geotification
    fileprivate func handleEvent(forRegion region: CLRegion!) {
        
        guard let noteStr = note(fromRegionIdentifier: region.identifier) else {
            return
        }
        
        let noteArr = noteStr.split("[-]|[-]")
        let title = noteArr[0]
        let message = noteArr[1]
        
        // Show an alert if application is active
        if UIApplication.shared.applicationState == .active {
            window?.rootViewController?.showAlertWithTitle(title: title, message: message)
        } else {
            // Otherwise present a local notification
            let notification = UILocalNotification()
            
            notification.alertTitle = title
            notification.alertBody = message
            notification.soundName = "Default"
            UIApplication.shared.presentLocalNotificationNow(notification)
        }
        
    }
    
    func note(fromRegionIdentifier identifier: String) -> String? {
        let savedItems = UserDefaults.standard.array(forKey: PreferencesKeys.savedItems) as? [NSData]
        let geotifications = savedItems?.map { NSKeyedUnarchiver.unarchiveObject(with: $0 as Data) as? Geotification }
        let index = geotifications?.index { $0?.identifier == identifier }
        return index != nil ? geotifications?[index!]?.note : nil
    }
    
    //func saveAllRegons
}


extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            
            handleEvent(forRegion: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
    
    
    
    
    func addGeoBy(additionalData:[AnyHashable:Any])  {
//        let coordinate = CLLocationCoordinate2D(latitude: additionalData["lat"] as! CLLocationDegrees, longitude: additionalData["long"] as! CLLocationDegrees)
//        let radius = additionalData["radius"] as! Double
//        let identifier = NSUUID().uuidString
//        let note = additionalData["content"] as! String
//        let eventType =  EventType(rawValue:additionalData["eventType"]as!String)!
//        let clampedRadius = min(radius, self.locationManager.maximumRegionMonitoringDistance)
//        let senderName =  additionalData["senderName"] as! String
//        let geotification = Geotification(coordinate: coordinate, radius: clampedRadius, identifier: identifier, note: note , eventType: eventType, senderName: senderName)
//        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
//            print("Geofencing is not supported on this device!")
//        }
//        
//        if CLLocationManager.authorizationStatus() != .authorizedAlways {
//            print("your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
//        }
//        
//        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
//        // 2
//        region.notifyOnEntry = (geotification.eventType == .onEntry)
//        region.notifyOnExit = !region.notifyOnEntry
//        // 4
//        locationManager.startMonitoring(for: region)
       
    }
    
}

